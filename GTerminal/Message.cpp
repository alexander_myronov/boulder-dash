#include "Message.h"

#include "System.h"

Message::~Message() {
    MessageBox(NULL, ::widen(str()).c_str(), ::widen(title).c_str(), MB_OK);
}
