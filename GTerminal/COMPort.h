#ifndef COMPort_h
#define COMPort_h

#include <string>
#include "System.h"

class COMPort {
	COMPort(const COMPort&);
	void operator=(const COMPort&);

public:
	COMPort(const std::string& name);
	~COMPort();

private:
	HANDLE handle;
};

#endif
