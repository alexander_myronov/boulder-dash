#include "System.h"

#include <string.h>

size_t doWidening(const char* str, size_t n, wchar_t* wstr, size_t wn) {
    wn = (size_t)MultiByteToWideChar(CP_UTF8, 0, str, (int)n, wstr, (int)wn);
    wstr[wn] = 0;
    return wn;
}

size_t doNarrowing(const wchar_t* wstr, size_t wn, char* str, size_t n) {
    n = (size_t)WideCharToMultiByte(CP_UTF8, 0, wstr, (int)wn, str, (int)n, NULL, NULL);
    str[n] = 0;
    return n;
}
