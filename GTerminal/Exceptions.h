#ifndef Exceptions_h
#define Exceptions_h

#include <exception>
#include <iosfwd>
#include <string>

class Exception : public std::exception {
public:
    Exception(const char* title) : title(title), whatStorage(0) {}
    virtual ~Exception() throw();

    const char* const title;
    
    virtual void describe(std::ostream& os) const throw() = 0;

    virtual const char* what() const throw();

private:
    mutable char* whatStorage;
};

inline std::ostream& operator<< (std::ostream& os, const Exception& e) {
    e.describe(os);
    return os;
}

class WinApiException : public Exception {
public:
    WinApiException(const char* function = 0);

    virtual void describe(std::ostream& os) const throw();

    const char* const function;
    const int errorNumber;
};

class SDLLikeException : public Exception {
public:
    SDLLikeException(const char* title, const char* message,
		const char* function);

    virtual void describe(std::ostream& os) const throw();

    const char* const function;
    const char* const errorMessage;
};

class SDLException : public SDLLikeException {
public:
    SDLException(const char* function = 0);
};

class TTFException : public SDLLikeException {
public:
    TTFException(const char* function = 0);
};

#endif
