#ifndef Message_h
#define Message_h

#include <sstream>
#include <string>

class Message : public std::ostringstream {
public:
    Message(const std::string& title) : title(title) {}
    ~Message();

private:
    const std::string& title;
};

#endif
