#ifndef Terminal_h
#define Terminal_h

#include <stdint.h>
#include <string.h>

// Instructions
#define TERMINAL_REG8_WRITE 0x00 // 0r dd: r: reg, d: data
#define TERMINAL_REG8_WRITE_LENGTH 2u
#define TERMINAL_REG8_WRITE_REGMASK 0xF

#define TERMINAL_REG16_WRITE 0x10 // 1r dd dd: r: reg, d: data
#define TERMINAL_REG16_WRITE_LENGTH 3u
#define TERMINAL_REG16_WRITE_REGMASK 0xF

#define TERMINAL_WRITE_ABS 0x20 // 2s aa aa dd .. dd: s: size-1, a: address, d: data
#define TERMINAL_WRITE_ABS_LENGTH 4u // + s
#define TERMINAL_WRITE_ABS_SIZEMASK 0xF

#define TERMINAL_WRITE_ABSS 0xF1 // F1 ss aa aa dd .. dd: s: size-1, a: address, d: data
#define TERMINAL_WRITE_ABSS_LENGTH 5u // + s

#define TERMINAL_WRITE_ABSB 0xF2 // F2 ss ss aa aa dd .. dd: s: size-1, a: address, d: data
#define TERMINAL_WRITE_ABSB_LENGTH 6u // + s

#define TERMINAL_WRITE_REL 0x40 // b01ssDaaa aa dd .. dd: s: size-1, D: direction, a: address, d: data
#define TERMINAL_WRITE_REL_LENGTH 3u // + s
#define TERMINAL_WRITE_REL_SIZEMASK 0x3
#define TERMINAL_WRITE_REL_ADDRMASK 0x7FF

#define TERMINAL_WRITE_RELS 0x30 // b0011Daaa ss aa dd .. dd: D: direction, a: address, s: size-1, d: data
#define TERMINAL_WRITE_RELS_LENGTH 4u // + s
#define TERMINAL_WRITE_RELS_ADDRMASK 0x7FF

#define TERMINAL_WRITE_SWAP 0x80 // b1000Daaa aa: D: direction, a: address
#define TERMINAL_WRITE_SWAP_LENGTH 2u
#define TERMINAL_WRITE_SWAP_ADDRMASK 0x7FF

#define TERMINAL_RESET 0xF0
#define TERMINAL_RESET_LENGTH 1u

// Registers
// 8-bit
#define TERMINAL_R8_BGCOLOR 0
#define TERMINAL_R8_DIVISOR 1
#define TERMINAL_R8_NUM 2

// 16-bit
#define TERMINAL_R16_PATTERN_ADDR 0
#define TERMINAL_R16_MAP_ADDR 1
#define TERMINAL_R16_MAP_STRIDE 2
#define TERMINAL_R16_MAP_LINES 3
#define TERMINAL_R16_OFFSET_X 4
#define TERMINAL_R16_OFFSET_Y 5
#define TERMINAL_R16_CONFIGURATION 6
#define TERMINAL_R16_RELATIVE_ADDR 7
#define TERMINAL_R16_DIR0_STRIDE 8
#define TERMINAL_R16_DIR1_STRIDE 9
#define TERMINAL_R16_NUM 10

// Configuration register:
#define TERMINAL_CONF_ENABLE 0
#define TERMINAL_CONF_DIVISOR_SIGNAL 1

#endif
