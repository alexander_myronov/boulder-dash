#ifndef CircularBuffer_h
#define CircularBuffer_h

#include <assert.h>
#include <string.h>
#include <vector>

template <typename T>
class CircularBuffer {
	static size_t NextPOW(size_t n) {
		if (n == 0) {
			return 1;
		}
		--n;
		n |= (n >> 1);
		n |= (n >> 2);
		n |= (n >> 4);
		n |= (n >> 8);
		n |= (n >> 16);
// XXX Pretty unsafe.
#ifdef _WIN64
		n |= (n >> 32);
#endif
		++n;
		return n;
	}

public:
	CircularBuffer() : buffer(1), start(0), stop(0) {}
	CircularBuffer(size_t n) : buffer(NextPOW(n)), start(0), stop(0) {}

	void makeEmpty() {
		start = stop = 0;
	}

	void clear() {
		makeEmpty();
		buffer.clear();
		buffer.resize(1);
	}

	bool empty() const {
		return start == stop;
	}

	size_t size() const {
		return (stop - start) & (buffer.size()-1);
	}

	void push(const T& e) {
		size_t i = stop;
		increment(i);
		if (i == start) {
			resizeBuffer(buffer.size()*2);
		}
		buffer[stop] = e;
		increment(stop);
	}

	T pop() {
		assert(!empty());
		size_t i = start;

		increment(start);

		return buffer[i];
	}

	T top() {
		assert(!empty());
		return buffer[start];
	}

	void pop(T* array, size_t asize) {
		assert(size() >= asize);
		//TODO
		for (size_t i = 0; i < asize; ++i) {
			array[i] = pop();
		}
	}

	void top(T* array, size_t asize) {
		assert(size() >= asize);
		//TODO
		size_t it = start;

		for (size_t i = 0; i < asize; ++i) {
			array[i] = buffer[it];
			increment(it);
		}
	}

	void resize(size_t nsize) {
		nsize = NextPOW(nsize+1);
		assert(nsize > size());

		if (nsize != buffer.size()) {
			resizeBuffer(nsize);
		}
	}

	void fit() {
		resize(size());
	}

private:
	std::vector<T> buffer;
	size_t start, stop;

	void increment(size_t& ptr) {
		ptr = (ptr+1) & (buffer.size() - 1);
	}

	void resizeBuffer(size_t nsize) {
		std::vector<T> nbuffer(nsize);
		copyTo(nbuffer);
		buffer.swap(nbuffer);
	}

	void copyTo(std::vector<T>& nbuffer) {
		if (empty()) {
			return;
		}
		assert(nbuffer.size() > size());

		if (start < stop) {
			memcpy(&nbuffer[0], &buffer[start], (stop-start) * sizeof(T));
		} else {
			const size_t firstSize = buffer.size() - start;
			memcpy(&nbuffer[0], &buffer[start], firstSize * sizeof(T));
			memcpy(&nbuffer[firstSize], &buffer[0], stop * sizeof(T));
		}
	}
};

#endif
