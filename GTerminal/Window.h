#ifndef Window_h
#define Window_h

#include <deque>
#include <vector>

#include <SDL.h>
#include <SDL_ttf.h>
#include <stdint.h>

class Window {
	static const uint32_t MemorySize = 64 * 1024;
	static const int16_t WindowWidth = 320;
	static const int16_t WindowHeight = 240;

	static const int16_t MaxMapStride = 256;
	static const int16_t MaxMapLines = 256;
	static const int16_t MaxMapSize = 16 * 1024;
    
    static const double FPS;
	static const double MSPF;

	static const bool DoublePixels = true;

	struct Pattern {
		static const int32_t Width = 16;
		static const int32_t Height = 16;

		// Every pixel is represented by 4 bits, LSB first.
		typedef uint8_t Line[Width/2];
		Line lines[Height];
	};

	static_assert((sizeof(Pattern) & (sizeof(Pattern)-1)) == 0, "sizeof(Pattern) is not POT");

	static const uint32_t PatternsSize = sizeof(Pattern)*256;

public:
	struct Text {
		// Returns true, whether the text changed.
		typedef bool GetText(const Text& text, char* buffer);

		GetText* getText;
		size_t bufferSize;
		unsigned position[2];
		SDL_Color color;
		bool visible;
	};

	Window(const std::string& root);
	~Window();

	const unsigned* getWindowSize() const { return surfaceSize; }

    uint8_t wait();
	bool flip();

	void writeInstruction(const uint8_t* buf, size_t bytes);

	void reset();
	void writeReg8(uint8_t reg, uint8_t data);
	void writeReg16(uint8_t reg, uint16_t data);
	void write(uint16_t addr, const uint8_t* buf, size_t bytes);
	void writeRelative(uint16_t raddr, bool direction, const uint8_t* buf, size_t bytes);
	void swap(uint16_t raddr, bool direction);

	typedef void HandleEvent(Window*, const SDL_Event*);
	void setHandleEvent(HandleEvent* he) { handleEvent = he; }

	void addText(Text& text);
	void removeText(Text& text);

private:
	struct TextInternal {
		Text* text;
		char* buffer;
		SDL_Surface* renderedText;
		size_t bufferSize;

		TextInternal() : text(0), buffer(0), renderedText(0), bufferSize(0) {}

		void assign(Text* text);
		void free();
		void render(TTF_Font* font, SDL_Surface* screen);

		friend bool operator == (const TextInternal& a, const TextInternal& b) {
			return a.text == b.text;
		}

		friend bool operator == (const TextInternal& a, Text* text) {
			return a.text == text;
		}
	};

    void popMany(uint8_t* b, size_t s) {
        for (size_t i = 0; i < s; ++i) {
            b[i] = buffer.front();
            buffer.pop_front();
        }
    }

	std::string root;
	SDL_Surface* surface;
	TTF_Font* font;
	uint8_t* memory;
	std::deque<uint8_t> buffer;
	uint32_t colors[16];
	unsigned surfaceSize[2];
	uint32_t startTick;
	unsigned frame;
	HandleEvent* handleEvent;
	std::vector<TextInternal> texts;
    unsigned count, countI;
    double interval;
	
	uint16_t patternAddress;
	uint16_t mapAddress;
	int16_t mapStride, mapLines;
	int16_t offset[2];
	int16_t windowSize[2];
	uint16_t relativeAddress;
	uint16_t directionStride[2];
	uint8_t backgroundColor;
    uint8_t divisor;
	bool enabled;
    bool divisorEnabled;

	uint32_t* windowLine(unsigned line);
	Pattern* pattern(uint8_t num);
	uint8_t& tile(int16_t x, int16_t y);
	
	void initSDL();
	void initTTF();
	void initColors();

    void calculateInterval();

	bool configurePattern(uint32_t address);
	bool configureMap(uint32_t address, uint16_t stride, uint16_t lines);

	void clearScreen();
	void drawPixel(uint32_t* line1, uint32_t* line2, uint32_t& x, uint32_t c);

	void drawTiles();
	void drawFullTiles(const int32_t minTile[2], const int32_t maxTile[2],
					   const int32_t start[2]);
	void drawPartHorizontalTiles(int32_t tileY, int32_t minTileX, int32_t maxTileX,
								 const int32_t start[2],
								 uint32_t minY, uint32_t maxY);
	void drawPartVerticalTiles(int32_t tileX, int32_t minTileY, int32_t maxTileY,
							   const int32_t start[2],
							   uint32_t minX, uint32_t maxX);
	void drawPartTiles(int32_t tileX, int32_t tileY,
					   const int32_t start[2],
					   const uint32_t min[2], const uint32_t max[2]);

	bool executeInstruction();
};

#endif
