#include "Exceptions.h"

#include "System.h"

#include <SDL.h>
#include <SDL_ttf.h>

#include <ostream>
#include <sstream>

Exception::~Exception() {
    if (whatStorage) {
        delete[] whatStorage;
    }
}

const char* Exception::what() const {
    if (!whatStorage) {
        std::ostringstream os;
        describe(os);
        size_t size = os.str().size();
        whatStorage = new char[size+1];
        memcpy(whatStorage, os.str().data(), size);
        whatStorage[size] = 0;
    }
    return whatStorage;
}

WinApiException::WinApiException(const char* function) :
  Exception("WinApi Error"),
  function(function),
  errorNumber(GetLastError()) {
}

void WinApiException::describe(std::ostream& os) const {
    os << "WinApi error";
    if (function) {
        os << " from function " << function;
    }
    LPWSTR msg = 0;
    FormatMessage(FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM
        | FORMAT_MESSAGE_IGNORE_INSERTS, NULL, errorNumber,
        MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), (LPWSTR)&msg, 0, NULL);
    
    os << " (" << errorNumber << "):\n" << (msg ? narrow(msg) : "Unknown error.");
    if (msg) {
        LocalFree(msg);
    }
}

SDLLikeException::SDLLikeException(const char* title,
								   const char* message,
								   const char* function) :
  Exception(title),
  function(function),
  errorMessage(message) {
}

void SDLLikeException::describe(std::ostream& os) const {
    os << title;
    if (function) {
        os << " from function " << function;
    }
	os << ": " << errorMessage << ".";
}

SDLException::SDLException(const char* function) :
	SDLLikeException("SDL Error", SDL_GetError(), function) {}

TTFException::TTFException(const char* function) :
	SDLLikeException("TTF Error", TTF_GetError(), function) {}
