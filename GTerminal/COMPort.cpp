#include "COMPort.h"

#include "Exceptions.h"
#include <assert.h>

COMPort::COMPort(const std::string& name) :
  handle(nullptr) {
	handle = CreateFile(widen(name).c_str(), GENERIC_READ | GENERIC_WRITE, 0,
		nullptr, OPEN_EXISTING, FILE_FLAG_OVERLAPPED, nullptr);
	if (handle == INVALID_HANDLE_VALUE) {
		throw WinApiException("CreateFile");
	}
}

COMPort::~COMPort() {
	CloseHandle(handle);
}
