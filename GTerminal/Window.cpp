#include "Window.h"
#include "Exceptions.h"

#include "terminal.h"

#include <algorithm>
#include <assert.h>
#include <string.h>

#include <windows.h>

const double Window::FPS = 20;
const double Window::MSPF = 1000/FPS;

#include <fstream>
#include <string>

std::ofstream logger("1.log",std::ios::out);

Window::Window(const std::string& root) :
	root(root),
	surface(0),
	font(0),
	memory(0),
	startTick(0),
	frame(0),
	handleEvent(0),
	patternAddress(0),
	mapAddress(0),
	mapStride(0),
	mapLines(0),
	relativeAddress(0),
	backgroundColor(0),
    divisor(0),
	enabled(false),
    divisorEnabled(false) {

	offset[0] = offset[1] = 0;
	directionStride[0] = directionStride[1] = 0;

	windowSize[0] = WindowWidth;
	windowSize[1] = WindowHeight;

	initSDL();
	initColors();
	initTTF();

	memory = new uint8_t[MemorySize];
	memset(memory, 0, MemorySize);

    startTick = GetTickCount();
    calculateInterval();
}

void Window::initSDL() {
    timeBeginPeriod(1);

	if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_TIMER) < 0) {
		throw SDLException("SDL_Init");
	}

	for (int i = 0; i < 2; ++i) {
		surfaceSize[i] = windowSize[i] * (DoublePixels ? 2 : 1);
	}

	surface = SDL_SetVideoMode(surfaceSize[0], surfaceSize[1], 32, SDL_SWSURFACE);
	if (!surface) {
		throw SDLException("SDL_SetVideoMode");
	}
}

void Window::initTTF() {
	if (TTF_Init() < 0) {
		throw TTFException("TTF_Init");
	}
	
	char path[512];
	sprintf_s(path, "%s/FreeMonoBold.ttf", root.c_str());

	font = TTF_OpenFont(path, 24);
	if (!font) {
		throw TTFException("TTF_OpenFont");
	}
}

void Window::initColors() {
	colors[ 0] = SDL_MapRGB(surface->format, 0x00, 0x00, 0x00);
	colors[ 1] = SDL_MapRGB(surface->format, 0xFF, 0xFF, 0xFF);
	colors[ 2] = SDL_MapRGB(surface->format, 0x89, 0x40, 0x36);
	colors[ 3] = SDL_MapRGB(surface->format, 0x7A, 0xBF, 0xC7);
	colors[ 4] = SDL_MapRGB(surface->format, 0x8A, 0x46, 0xAE);
	colors[ 5] = SDL_MapRGB(surface->format, 0x68, 0xA9, 0x41);
	colors[ 6] = SDL_MapRGB(surface->format, 0x3E, 0x31, 0xA2);
	colors[ 7] = SDL_MapRGB(surface->format, 0xD0, 0xDC, 0x71);
	colors[ 8] = SDL_MapRGB(surface->format, 0x90, 0x5F, 0x25);
	colors[ 9] = SDL_MapRGB(surface->format, 0x5C, 0x47, 0x00);
	colors[10] = SDL_MapRGB(surface->format, 0xBB, 0x77, 0x6D);
	colors[11] = SDL_MapRGB(surface->format, 0x55, 0x55, 0x55);
	colors[12] = SDL_MapRGB(surface->format, 0x80, 0x80, 0x80);
	colors[13] = SDL_MapRGB(surface->format, 0xAC, 0xEA, 0x88);
	colors[14] = SDL_MapRGB(surface->format, 0x7C, 0x70, 0xDA);
	colors[15] = SDL_MapRGB(surface->format, 0xAB, 0xAB, 0xAB);
}

Window::~Window() {
	for (size_t i = 0, S = texts.size(); i < S; ++i) {
		texts[i].free();
	}

	SDL_Quit();
	TTF_CloseFont(font);
	TTF_Quit();

	delete[] memory;
	logger.close();
}

void Window::addText(Text& text) {
	auto it = std::find(texts.begin(), texts.end(), &text);
	if (it != texts.end()) {
		return;
	}
	texts.push_back(TextInternal());
	texts.back().assign(&text);
}

void Window::removeText(Text& text) {
	auto it = std::find(texts.begin(), texts.end(), &text);
	if (it != texts.end()) {
		return;
	}
	it->free();
	texts.erase(it);
}

void Window::TextInternal::assign(Text* text) {
	free();

	this->text = text;
}

void Window::TextInternal::free() {
	if (buffer) {
		::free(buffer);
		buffer = 0;
		bufferSize = 0;
	}
	if (renderedText) {
		SDL_FreeSurface(renderedText);
		renderedText = 0;
	}

	text = 0;
}

void Window::TextInternal::render(TTF_Font* font, SDL_Surface* screen) {
	assert(text);

	if (!text->visible) {
		return;
	}

	if (!text->bufferSize) {
		return;
	}

	if (!bufferSize) {
		bufferSize = text->bufferSize;
		buffer = (char*)malloc(bufferSize);
	} else if (bufferSize != text->bufferSize) {
		bufferSize = text->bufferSize;
		buffer = (char*)realloc(buffer, bufferSize);
	}

	if (text->getText(*text, buffer)) {
		if (renderedText) {
			SDL_FreeSurface(renderedText);
		}
		renderedText = TTF_RenderText_Blended(font, buffer, text->color);
	}

	SDL_Rect rect = {text->position[0], text->position[1], 0, 0};

	SDL_BlitSurface(renderedText, 0, screen, &rect);
}

void Window::calculateInterval() {
    if (!divisorEnabled) {
        count = 1;
    } else {
        count = 1 + divisor;
    }

    interval = MSPF / count;
    countI = 0;
}

uint8_t Window::wait() {
	const uint32_t nextTick = startTick + uint32_t(MSPF*frame + interval*countI);
	uint32_t ticks = GetTickCount();
    logger << "Before: " << ticks << std::endl;
	while (nextTick > ticks) {
        Sleep(nextTick - ticks);
        ticks = GetTickCount();
	}
    logger << "After: " << ticks << std::endl;
    ++countI;

    return count - countI;
}

bool Window::flip() {
    ++frame;

	SDL_Event event;
	while ((SDL_PollEvent(&event))) {
		switch (event.type) {
		case SDL_QUIT:
			return false;
		default:
			if (handleEvent) {
				(*handleEvent)(this, &event);
			}
			break;
		}
	}

	while (true) {
		if (!executeInstruction()) {
			break;
		}
	}
    
    calculateInterval();

	if (SDL_MUSTLOCK(surface)) {
		SDL_LockSurface(surface);
	}

	clearScreen();

	if (enabled &&
		(offset[0] < mapStride*Pattern::Width && offset[0] > -windowSize[0]) &&
		(offset[1] < mapLines*Pattern::Height && offset[1] > -windowSize[1])) {

		drawTiles();
	}

	if (SDL_MUSTLOCK(surface)) {
		SDL_UnlockSurface(surface);
	}

	for (size_t i = 0, S = texts.size(); i < S; ++i) {
		texts[i].render(font, surface);
	}

	SDL_UpdateRect(surface, 0, 0, 0, 0);

	return true;
}


void Window::writeInstruction(const uint8_t* buf, size_t bytes) {
	for (size_t i = 0; i < bytes; ++i) {
		buffer.push_back(buf[i]);
	}
}

void Window::reset() {
	configurePattern(0);
	configureMap(0, 1, 1);
	offset[0] = offset[1] = 0;
	relativeAddress = 0;
	directionStride[0] = directionStride[1] = 0;
	backgroundColor = 0;
    divisor = 0;
	enabled = false;
    divisorEnabled = false;
}

void Window::writeReg8(uint8_t reg, uint8_t data) {
	switch (reg) {
	case TERMINAL_R8_BGCOLOR:
		backgroundColor = (data & 0x0F);
		break;
    case TERMINAL_R8_DIVISOR:
        divisor = data;
        break;
	default:
		// Ignore.
		break;
	}
}

void Window::writeReg16(uint8_t reg, uint16_t data) {
	switch (reg) {
	case TERMINAL_R16_PATTERN_ADDR:
		configurePattern(data);
		break;
	case TERMINAL_R16_MAP_ADDR:
		configureMap(data, mapStride, mapLines);
		break;
	case TERMINAL_R16_MAP_STRIDE:
		configureMap(mapAddress, data, mapLines);
		break;
	case TERMINAL_R16_MAP_LINES:
		configureMap(mapAddress, mapStride, data);
		break;
	case TERMINAL_R16_OFFSET_X:
		offset[0] = data;
		break;
	case TERMINAL_R16_OFFSET_Y:
		offset[1] = data;
		break;
	case TERMINAL_R16_CONFIGURATION:
		enabled = !!(data & (1 << TERMINAL_CONF_ENABLE));
        divisorEnabled = !!(data & (1 << TERMINAL_CONF_DIVISOR_SIGNAL));
		break;
	case TERMINAL_R16_RELATIVE_ADDR:
		relativeAddress = data;
		break;
	case TERMINAL_R16_DIR0_STRIDE:
		directionStride[0] = data;
		break;
	case TERMINAL_R16_DIR1_STRIDE:
		directionStride[1] = data;
		break;
	default:
		// Ignore.
		break;
	}
}

void Window::write(uint16_t addr, const uint8_t* buf, size_t bytes) {
	assert(bytes <= MemorySize);

	if (addr + bytes <= MemorySize) {
		memcpy(&memory[addr], buf, bytes);
		return;
	}

	const size_t firstSize = MemorySize - addr;
	memcpy(&memory[addr], buf, firstSize);
	memcpy(&memory[0], buf+firstSize, bytes - firstSize);
}

static void memcpyWithDestStride(uint8_t* dest, size_t destStride, const uint8_t* src, size_t bytes) {
    for (size_t i = 0; i < bytes; ++i) {
        *dest = *src++;
        dest += destStride;
    }
}

void Window::writeRelative(uint16_t addr, bool direction, const uint8_t* buf, size_t bytes) {
    const uint16_t stride = directionStride[direction ? 1 : 0];
    if (stride == 0) {
        // Wrong operation.
        return;
    }

	assert(bytes*stride <= MemorySize);

    addr += relativeAddress;

	if (addr + bytes*stride <= MemorySize) {
		memcpyWithDestStride(&memory[addr], stride, buf, bytes);
		return;
	}

	const size_t firstSize = (MemorySize - addr) / stride;
    size_t secondAddr = stride - (MemorySize - addr) % stride;
    secondAddr = (secondAddr == stride ? 0 : secondAddr);

	memcpyWithDestStride(&memory[addr], stride, buf, firstSize);
	memcpy(&memory[secondAddr], buf+firstSize, bytes - firstSize);
}

void Window::swap(uint16_t addr, bool direction) {
    const uint16_t stride = directionStride[direction ? 1 : 0];
    if (stride == 0) {
        // Wrong operation.
        return;
    }

	const uint16_t addr0 = relativeAddress + addr;
	const uint16_t addr1 = addr0 + stride;
	std::swap(memory[addr0], memory[addr1]);
}

uint32_t* Window::windowLine(unsigned line) {
	return (uint32_t*)(&((uint8_t*)surface->pixels)[line*surface->pitch]);
}

Window::Pattern* Window::pattern(uint8_t num) {
	uint32_t addr = patternAddress + num * sizeof(Pattern);
	if (addr + sizeof(Pattern) > MemorySize) {
		return nullptr;
	}
	return (Pattern*)(&memory[addr]);
}

uint8_t& Window::tile(int16_t x, int16_t y) {
	assert(x < mapStride);
	assert(y < mapLines);

	uint32_t addr = mapAddress + y*mapStride + x;
	return *(uint8_t*)(&memory[addr]);
}

bool Window::configurePattern(uint32_t address) {
	if ((address & ~sizeof(Pattern)) != address ||
		address >= MemorySize || address + sizeof(Pattern) > MemorySize) {

		return false;
	}

	patternAddress = address;

	return true;
}

bool Window::configureMap(uint32_t address, uint16_t stride, uint16_t lines) {
	if (stride == 0 && lines == 0) {
		// Ok.
	} else if ((stride == 0 || lines == 0) ||
			   (stride > MaxMapStride || lines > MaxMapLines) ||
			   stride * lines > MaxMapSize) {

		return false;
	}

	if (address >= MemorySize || address + stride * lines > MemorySize) {

		return false;
	}

	mapAddress = address;
	mapStride = stride;
	mapLines = lines;

	return true;
}

void Window::clearScreen() {
	for (uint32_t y = 0; y < uint32_t(surface->h); ++y) {
		for (uint32_t x = 0; x < uint32_t(surface->w); ++x) {
			windowLine(y)[x] = colors[backgroundColor];
		}
	}
}

void Window::drawPixel(uint32_t* line1, uint32_t* line2, uint32_t& x, uint32_t c) {
	if (DoublePixels) {
		line1[x+0] = c;
		line1[x+1] = c;
		line2[x+0] = c;
		line2[x+1] = c;
		x += 2;
	} else {
		line1[x] = c;
		x += 1;
	}
}

void Window::drawTiles() {
/* Window:
/==========================windowSize===========================\

+===+========+========+========+========+========+========+=====+
|mT |minTile |        |        |        |        |maxTile |maxT |
|-1 |        |        |        |        |        | -1     |     |
+===Z========+========+========+========+========+========+=====+
|   |        |        |        |        |        |        |     |
|   |        |        |        |        |        |        |     |
|   |        |        |        |        |        |        |     |
|   |        |        |        |        |        |        |     |
|   |        |        |        |        |        |        |     |
+===+========+========+========+========+========+========+=====+
|   |        |        |        |        |        |        |     |
|   |        |        |        |        |        |        |     |
|   |        |        |        |        |        |        |     |
|   |        |        |        |        |        |        |     |
|   |        |        |        |        |        |        |     |
+===+========+========+========+========+========+========V=====+
|   |        |        |        |        |        |        |     |
|   |        |        |        |        |        |        |     |
|   |        |        |        |        |        |        |     |
+===+========+========+========+========+========+========+=====+

Z, V = fullStart, fullEnd

*/
	const int32_t minTile[2] = {
		offset[0] <= 0 ? 0 : (offset[0]-1) / Pattern::Width + 1,
		offset[1] <= 0 ? 0 : (offset[1]-1) / Pattern::Height + 1
	};
	const int32_t maxTile[2] = {
		std::min<uint32_t>((offset[0] + windowSize[0]) / Pattern::Width, mapStride),
		std::min<uint32_t>((offset[1] + windowSize[1]) / Pattern::Height, mapLines)
	};
	const int32_t offsetMod[2] = {
		offset[0] & (Pattern::Width-1),
		offset[1] & (Pattern::Height-1)
	};
	const int32_t fullStart[2] = {
		offset[0] < 0 ? -offset[0] : (offsetMod[0] == 0 ? 0 : Pattern::Width - offsetMod[0]),
		offset[1] < 0 ? -offset[1] : (offsetMod[1] == 0 ? 0 : Pattern::Height - offsetMod[1])
	};
	const int32_t fullEnd[2] = {
		fullStart[0] + (maxTile[0] - minTile[0]) * Pattern::Width,
		fullStart[1] + (maxTile[1] - minTile[1]) * Pattern::Height
	};

	drawFullTiles(minTile, maxTile, fullStart);

	if (offsetMod[1] != 0) {
		if (minTile[1] > 0) {
			const int32_t topStart[2] = {
				fullStart[0],
				0
			};
			drawPartHorizontalTiles(minTile[1]-1, minTile[0], maxTile[0],
									topStart, offsetMod[1], Pattern::Height);
		}

		if (maxTile[1] < mapLines) {
			const int32_t bottomStart[2] = {
				fullStart[0],
				fullEnd[1]
			};
			drawPartHorizontalTiles(maxTile[1], minTile[0], maxTile[0],
									bottomStart, 0, offsetMod[1]);
		}
	}

	if (offsetMod[0] != 0) {
		if (minTile[0] > 0) {
			const int32_t topStart[2] = {
				0,
				fullStart[1]
			};
			drawPartVerticalTiles(minTile[0]-1, minTile[1], maxTile[1],
								  topStart, offsetMod[0], Pattern::Width);
		}

		if (maxTile[0] < mapStride) {
			const int32_t bottomStart[2] = {
				fullEnd[0],
				fullStart[1]
			};
			drawPartVerticalTiles(maxTile[0], minTile[1], maxTile[1],
								  bottomStart, 0, offsetMod[0]);
		}
	}

	if (offsetMod[0] != 0 && offsetMod[1] != 0) {
		if (minTile[0] > 0 && minTile[1] > 0) {
			const int32_t topLeftStart[2] = {
				0,
				0
			};
			const uint32_t min[2] = {
				offsetMod[0],
				offsetMod[1]
			};
			const uint32_t max[2] = {
				Pattern::Width,
				Pattern::Height
			};
			drawPartTiles(minTile[0]-1, minTile[1]-1,
						  topLeftStart, min, max);
		}

		if (maxTile[0] < mapStride && minTile[1] > 0) {
			const int32_t topRightStart[2] = {
				fullEnd[0],
				0
			};
			const uint32_t min[2] = {
				0,
				offsetMod[1]
			};
			const uint32_t max[2] = {
				offsetMod[0],
				Pattern::Height
			};
			drawPartTiles(maxTile[0], minTile[1]-1,
						  topRightStart, min, max);
		}

		if (minTile[0] > 0 && maxTile[1] < mapLines) {
			const int32_t bottomLeftStart[2] = {
				0,
				fullEnd[1]
			};
			const uint32_t min[2] = {
				offsetMod[0],
				0
			};
			const uint32_t max[2] = {
				Pattern::Width,
				offsetMod[1]
			};
			drawPartTiles(minTile[0]-1, maxTile[1],
						  bottomLeftStart, min, max);
		}

		if (maxTile[0] < mapStride && maxTile[1] < mapLines) {
			const int32_t bottomRightStart[2] = {
				fullEnd[0],
				fullEnd[1]
			};
			const uint32_t min[2] = {
				0,
				0
			};
			const uint32_t max[2] = {
				offsetMod[0],
				offsetMod[1]
			};
			drawPartTiles(maxTile[0], maxTile[1],
						  bottomRightStart, min, max);
		}
	}
}

void Window::drawFullTiles(const int32_t minTile[2], const int32_t maxTile[2],
						   const int32_t start[2]) {
	int D = (DoublePixels ? 2 : 1);

	uint32_t wy = start[1]*D;
	for (int32_t ty = minTile[1]; ty < maxTile[1]; ++ty) {
		for (uint32_t py = 0; py < Pattern::Height; ++py) {
			uint32_t* wline1 = windowLine(wy+0);
			uint32_t* wline2 = windowLine(wy+1);

			uint32_t wx = start[0]*D;
			for (int32_t tx = minTile[0]; tx < maxTile[0]; ++tx) {
				Pattern* p = pattern(tile(tx, ty));
				Pattern::Line& tline = p->lines[py];

				for (uint32_t px = 0; px < Pattern::Width/2; ++px) {
					uint32_t c1 = colors[(tline[px] >> 0) & 0xF];
					uint32_t c2 = colors[(tline[px] >> 4) & 0xF];

					drawPixel(wline1, wline2, wx, c1);
					drawPixel(wline1, wline2, wx, c2);
				}
			}
			wy += D;
		}
	}
}

void Window::drawPartHorizontalTiles(int32_t tileY, int32_t minTileX, int32_t maxTileX,
								     const int32_t start[2],
								     uint32_t minY, uint32_t maxY) {
	int D = (DoublePixels ? 2 : 1);

	uint32_t wy = start[1]*D;
	for (uint32_t py = minY; py < maxY; ++py) {
		uint32_t* wline1 = windowLine(wy+0);
		uint32_t* wline2 = windowLine(wy+1);

		uint32_t wx = start[0]*D;
		for (int32_t tx = minTileX; tx < maxTileX; ++tx) {
			Pattern* p = pattern(tile(tx, tileY));
			Pattern::Line& tline = p->lines[py];

			for (uint32_t px = 0; px < Pattern::Width/2; ++px) {
				uint32_t c1 = colors[(tline[px] >> 0) & 0xF];
				uint32_t c2 = colors[(tline[px] >> 4) & 0xF];

				drawPixel(wline1, wline2, wx, c1);
				drawPixel(wline1, wline2, wx, c2);
			}
		}
		wy += D;
	}
}

void Window::drawPartVerticalTiles(int32_t tileX, int32_t minTileY, int32_t maxTileY,
							       const int32_t start[2],
							       uint32_t minX, uint32_t maxX) {
	int D = (DoublePixels ? 2 : 1);

	uint32_t wy = start[1]*D;
	for (int32_t ty = minTileY; ty < maxTileY; ++ty) {
		for (uint32_t py = 0; py < Pattern::Height; ++py) {
			uint32_t* wline1 = windowLine(wy+0);
			uint32_t* wline2 = windowLine(wy+1);

			uint32_t wx = start[0]*D;
			
			Pattern* p = pattern(tile(tileX, ty));
			Pattern::Line& tline = p->lines[py];

			uint32_t px = minX/2;
			if (minX & 1) {
				uint32_t c = colors[(tline[minX/2] >> 4) & 0xF];

				drawPixel(wline1, wline2, wx, c);
				px += 1;
			}
			for (; px < maxX/2; ++px) {
				uint32_t c1 = colors[(tline[px] >> 0) & 0xF];
				uint32_t c2 = colors[(tline[px] >> 4) & 0xF];

				drawPixel(wline1, wline2, wx, c1);
				drawPixel(wline1, wline2, wx, c2);
			}
			if (maxX & 1) {
				uint32_t c = colors[(tline[maxX/2] >> 0) & 0xF];

				drawPixel(wline1, wline2, wx, c);
			}
			wy += D;
		}
	}
}

void Window::drawPartTiles(int32_t tileX, int32_t tileY,
						   const int32_t start[2],
						   const uint32_t min[2], const uint32_t max[2]) {
	int D = (DoublePixels ? 2 : 1);

	uint32_t wy = start[1]*D;
	for (uint32_t py = min[1]; py < max[1]; ++py) {
		uint32_t* wline1 = windowLine(wy+0);
		uint32_t* wline2 = windowLine(wy+1);

		uint32_t wx = start[0]*D;
			
		Pattern* p = pattern(tile(tileX, tileY));
		Pattern::Line& tline = p->lines[py];

		uint32_t px = min[0]/2;
		if (min[0] & 1) {
			uint32_t c = colors[(tline[min[0]/2] >> 4) & 0xF];

			drawPixel(wline1, wline2, wx, c);
			px += 1;
		}
		for (; px < max[0]/2; ++px) {
			uint32_t c1 = colors[(tline[px] >> 0) & 0xF];
			uint32_t c2 = colors[(tline[px] >> 4) & 0xF];

			drawPixel(wline1, wline2, wx, c1);
			drawPixel(wline1, wline2, wx, c2);
		}
		if (max[0] & 1) {
			uint32_t c = colors[(tline[max[0]/2] >> 0) & 0xF];

			drawPixel(wline1, wline2, wx, c);
		}
		wy += D;
	}
}

bool Window::executeInstruction() {
	size_t size = buffer.size();
	if (size == 0) {
		return false;
	}

	uint8_t opcode = buffer.front();
	//logger<< std::hex << (unsigned)opcode <<std::endl;
#define INS0(x) case x:
#define INS1(x) INS0(x) INS0(x+1)
#define INS2(x) INS1(x) INS1(x+2)
#define INS3(x) INS2(x) INS2(x+4)
#define INS4(x) INS3(x) INS3(x+8)
#define INS5(x) INS4(x) INS4(x+16)
#define INS6(x) INS5(x) INS5(x+32)
#define INS7(x) INS6(x) INS6(x+64)
	
	bool dir;
	uint8_t reg, data8, sz;
	uint16_t data16, addr, sz16;
    uint8_t* bufp;
	uint8_t buf[256];

	switch (opcode) {
	INS0(TERMINAL_RESET)
		if (size < TERMINAL_RESET_LENGTH) {
			return false;
		}
		buffer.pop_front();
		reset();
		break;
	INS4(TERMINAL_REG8_WRITE)
		if (size < TERMINAL_REG8_WRITE_LENGTH) {
			return false;
		}
		reg = (buffer.front() & 0x0F);
        buffer.pop_front();
		data8 = buffer.front();
        buffer.pop_front();
		writeReg8(reg, data8);
		break;
	INS4(TERMINAL_REG16_WRITE)
		if (size < TERMINAL_REG16_WRITE_LENGTH) {
			return false;
		}
		reg = (buffer.front() & 0x0F);
        buffer.pop_front();
		data16 = buffer.front();
        buffer.pop_front();
		data16 |= (buffer.front() << 8);
        buffer.pop_front();
		writeReg16(reg, data16);
		break;
	INS4(TERMINAL_WRITE_ABS)
		sz = (buffer.front() & 0x0F);
		if (size < TERMINAL_WRITE_ABS_LENGTH + sz) {
			return false;
		}
		buffer.pop_front();
		addr = buffer.front();
        buffer.pop_front();
		addr |= (buffer.front() << 8);
        buffer.pop_front();

		popMany(buf, sz+1);
		write(addr, buf, sz+1);
		break;
	INS0(TERMINAL_WRITE_ABSS)
		if (size < 2) {
			return false;
		}
        buf[0] = buffer[0];
        buf[1] = buffer[1];
		sz = buf[1];
		if (size < TERMINAL_WRITE_ABSS_LENGTH + sz) {
			return false;
		}
        buffer.pop_front();
        buffer.pop_front();
		addr = buffer.front();
        buffer.pop_front();
		addr |= (buffer.front() << 8);
        buffer.pop_front();
		popMany(buf, sz+1);
		write(addr, buf, sz+1);
		break;
	INS0(TERMINAL_WRITE_ABSB)
		if (size < 3) {
			return false;
		}
        buf[0] = buffer[0];
        buf[1] = buffer[1];
        buf[2] = buffer[2];
		sz16 = buf[1];
        sz16 |= (buf[2] << 8);
		if (size < TERMINAL_WRITE_ABSS_LENGTH + sz16) {
			return false;
		}
        buffer.pop_front();
        buffer.pop_front();
        buffer.pop_front();

		addr = buffer.front();
        buffer.pop_front();
		addr |= (buffer.front() << 8);
        buffer.pop_front();
        bufp = new uint8_t[sz16+1];
		popMany(bufp, sz16+1);
		write(addr, bufp, sz16+1);
        delete[] bufp;
		break;
	INS6(TERMINAL_WRITE_REL)
		sz = ((buffer.front() >> 4) & 3);
		if (size < TERMINAL_WRITE_REL_LENGTH + sz) {
			return false;
		}
		dir = ((buffer.front() >> 3) & 1);
		addr = ((buffer.front() & 7) << 8);
        buffer.pop_front();
		addr |= buffer.front();
        buffer.pop_front();
		popMany(buf, sz+1);

		writeRelative(addr, dir, buf, sz+1);
		break;
	INS4(TERMINAL_WRITE_RELS)
		if (size < 2) {
			return false;
		}
        buf[0] = buffer[0];
        buf[1] = buffer[1];
		sz = buf[1];
		if (size < TERMINAL_WRITE_RELS_LENGTH + sz) {
			return false;
		}
		dir = ((buffer.front() >> 3) & 1);
		addr = ((buffer.front() & 7) << 8);
        buffer.pop_front();
		buffer.pop_front();
		addr |= buffer.front();
        buffer.pop_front();
		popMany(buf, sz+1);

		writeRelative(addr, dir, buf, sz+1);
		break;
	INS4(TERMINAL_WRITE_SWAP)
		if (size < TERMINAL_WRITE_SWAP_LENGTH) {
			return false;
		}
		dir = ((buffer.front() >> 3) & 1);
		addr = ((buffer.front() & 7) << 8);
        buffer.pop_front();
		addr |= buffer.front();
        buffer.pop_front();

		swap(addr, dir);
		break;
	default:
		// Wrong opcode: NOP
		buffer.pop_front();
		break;
	}
	return true;
}
