#include "Window.h"
#include "Message.h"
#include <SDL_main.h>
#include "../ComReader/ComReader.h"

static
std::string getProgramDirectory(const char* argv0) {
	std::string root(argv0);
	size_t pos = root.rfind('/');
	if (pos == std::string::npos) {
		pos = root.rfind('\\');
	}
	if (pos == std::string::npos) {
		return ".";
	}

	root.erase(pos);
	return root;
}

int main(int argc, char* argv[]) {
	try {
		std::string root(argv[0]);

		Window window(getProgramDirectory(argv[0]) + "/..");

		SDL_EnableKeyRepeat(1, 5);

		CRITICAL_SECTION cs;
		memset(&cs,0,sizeof(cs));
		InitializeCriticalSection(&cs);

		ComReader reader(wstring(L"COM3"),115200,TWOSTOPBITS,EVENPARITY,8,true,[&window,&cs](const void* data,int size)
		{
			EnterCriticalSection(&cs);
			window.writeInstruction((const uint8_t*)data,size);
			LeaveCriticalSection(&cs);
		});
		while(true)
		{
			uint8_t ret = window.wait();
			const uint8_t buff[] = {ret};
			reader.write(1,buff);
			if(ret == 0)
			{
				EnterCriticalSection(&cs);
				if(!window.flip())
					break;
				LeaveCriticalSection(&cs);
			}

		}
		DeleteCriticalSection(&cs);
	} catch (std::exception& e) {
		Message("Exception") << e.what();
	}
	return 0;
}
