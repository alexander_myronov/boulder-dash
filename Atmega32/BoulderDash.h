/*
 * BoulderDash.h - the game main file
 *
 * Created: 2013-06-11 12:12:30
 *  Author: Gustaw Smolarczyk
 */

#ifndef BoulderDash_h
#define BoulderDash_h

#include "Global.h"

enum player_state {
	player_wait,
	player_alive,
	player_dead,
	player_win
};

typedef struct {
	uint8_t position[2];
	uint16_t score;
	uint16_t diamonds;//already collected
	int8_t lives;
	uint8_t state;
	uint16_t time;
} player_t;

// Whether exit is visible.
extern bool player_exit_visible(void);

// Number of pixels (x/y) camera moves every frame.
#define PLAYER_CAMERA_STEP 9
#define PLAYER_CAMERA_STEP_DIV 2

extern void player_load_camera(int16_t coords[2]);

// Move camera a step towards player.
extern void player_move_camera(void);

// Set camera.
extern void player_set_camera(const int16_t coords[2]);

// Moves player according to dpad.
extern void player_move(void);

extern void player_show_numbers(void);

// Main Boulder Dash structure.
typedef struct {
	player_t player;
} boulder_dash_context_t;

extern boulder_dash_context_t boulder_dash_context;

// Initialize Boulder Dash.
extern void boulder_dash_init(void);

// Execute Boulder Dash. Should never return.
extern void boulder_dash_exec(void);

#endif /* BoulderDash_h */

