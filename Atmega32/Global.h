/*
 * Global.h - A header file containing global definitions.
 *
 * Created: 2013-04-16 16:53:23
 *  Author: Gustaw Smolarczyk
 */

#ifndef Global_h
#define Global_h

#include <stdint.h>

#ifdef BD_MOCK
#	define __flash /* nothing */
#	define _delay_us(x) /* no-op */
#	define _delay_ms(x) /* no-op */
#	define sei() /* no-op */
#	define cli() /* no-op */
#	ifndef __cplusplus
		typedef int bool;
#		define false 0
#		define true 1
#		define inline __inline
#	endif
#	include <assert.h>
#else
#   include <stdbool.h>
#	include <util/delay.h>
#	include <avr/interrupt.h>
#	define assert(x) /* nothing */
#endif

// A helper pair of macros to concatenate identifiers.
#define _CONCAT2(a, b) a ## b
#define _CONCAT(a, b) _CONCAT2(a, b)

// A forever loop (used for debugging or to wait for interrupt).
#define FOREVER_LOOP() do {				\
	volatile char __forever = 0;		\
	while (true) {						\
		++__forever;					\
	}									\
} while (false)

/** << Copied from mesa >>
 * Static (compile-time) assertion.
 * Basically, use COND to dimension an array.  If COND is false/zero the
 * array size will be -1 and we'll get a compilation error.
 */
#define STATIC_ASSERT(COND) do {		\
	(void)sizeof(char[1 - 2*!(COND)]);	\
} while (false)

#ifdef __GNUC__
#   define _LIKELY(x) __builtin_expect(!!(x), 1)
#   define _UNLIKELY(x) __builtin_expect(!!(x), 0)
#else
#   define _LIKELY(x) (x)
#   define _UNLIKELY(x) (x)
#endif

#endif /* Global_h */
