#include "USART.h"

#include <string.h>
#include <avr/io.h>

// The portion of bytes copied from indirect to direct buffer.
// Lower this value to lessen the latency of transfer when using
// the indirect buffer.
#define USART_INDIRECT_PORTION 32

// A bitmask that indicates __flash pointer (set on size field on usart_indirect).
#define USART_PTR_FLASH 0x8000u

// A structure for indirect pointer.
typedef struct {
	const char* ptr;
	uint16_t size;
} usart_indirect;

// An enumeration for indirect buffer state.
typedef enum {
	usart_state_unused,		// Indirect buffer is empty, free access to direct buffer.
	usart_state_pending,	// Indirect buffer holds data, but so does direct buffer.
	// The transfer will start when direct buffer is empty.
	// Direct buffer is disabled now.
	usart_state_busy		// Transfer from indirect buffer is in progress.
	// Direct buffer is disabled, because it's used by the transfer.
} usart_indirect_state_t;

// Buffers that hold data to transmit.
// Direct buffer holds current data. It's a simple array.
// Indirect buffer holds pointers to next data. It's a round buffer.
// When indirect buffer is used, any usage of direct buffer must be suspended
// until the driver is done using it (i.e. all data is transfered).
char usart_direct_buffer[USART_DIRECT_BUFFER_SIZE];
usart_indirect usart_indirect_buffer[USART_INDIRECT_BUFFER_SIZE];

// pos is a position of first empty place.
// apos is a position of first used place.
uint8_t usart_direct_buffer_pos;
uint8_t usart_direct_buffer_apos;

uint8_t usart_indirect_buffer_pos;
uint8_t usart_indirect_buffer_apos;

// The buffer is empty when pos == apos.
#define USART_BUFFER_EMPTY(w) (usart_##w##_buffer_apos == usart_##w##_buffer_pos)

// Zero buffer pointers.
#define USART_BUFFER_FREE(w) (usart_##w##_buffer_apos = usart_##w##_buffer_pos = 0)

// Advance a pos to the next place.
#define USART_BUFFER_NEXT(w, pos) (pos = (pos + 1) % USART_##w##_BUFFER_SIZE)

#define USART_DIRECT_BUFFER_EMPTY() USART_BUFFER_EMPTY(direct)
#define USART_DIRECT_BUFFER_FREE() USART_BUFFER_FREE(direct)
#define USART_DIRECT_BUFFER_NEXT(pos) (++(pos))
#define USART_DIRECT_BUFFER_LENGTH() (USART_DIRECT_BUFFER_SIZE - usart_direct_buffer_pos)

#define USART_INDIRECT_BUFFER_EMPTY() USART_BUFFER_EMPTY(indirect)
#define USART_INDIRECT_BUFFER_FREE() USART_BUFFER_FREE(indirect)
#define USART_INDIRECT_BUFFER_NEXT(pos) USART_BUFFER_NEXT(INDIRECT, pos)

// Indirect buffer state.
usart_indirect_state_t usart_indirect_state = usart_state_unused;

static uint16_t strlenC(const __flash char* s) {
	const __flash char* ptr = s;
	while (*ptr++);
	return (ptr - s) - 1;
}

static inline bool usart_udr_empty(void) {
	return (UCSRA >> UDRE) & 1;
}

#define USART_POPULATE_BODY(ptr, size)								\
	while (usart_direct_buffer_pos < USART_INDIRECT_PORTION) {		\
		usart_direct_buffer[usart_direct_buffer_pos++] = *ptr++;	\
		if (!--size) {												\
			goto ptr_end;											\
		}															\
	}

static bool usart_populate_direct_buffer(void) {
	if (USART_INDIRECT_BUFFER_EMPTY()) {
		// Nothing to transmit.
		return false;
	}
	
	do {
		usart_indirect* in = &usart_indirect_buffer[usart_indirect_buffer_apos];
		if (in->size & USART_PTR_FLASH) {
			// A flash pointer.
			const __flash char* flashPtr = (const __flash char*)(in->ptr);
			uint16_t flashSize = in->size & ~USART_PTR_FLASH;
			
			USART_POPULATE_BODY(flashPtr, flashSize);
			in->ptr = (const char*)flashPtr;
			in->size = flashSize | USART_PTR_FLASH;
		} else {
			// A normal pointer.
			USART_POPULATE_BODY(in->ptr, in->size);
		}
		return true;
		
	ptr_end:
		USART_INDIRECT_BUFFER_NEXT(usart_indirect_buffer_apos);
	} while (!USART_INDIRECT_BUFFER_EMPTY());
	
	return true;
}

void usart_configure(void) {
#if F_CPU == 16000000
	// 9600bps at 16MHz.
	UBRRH = 0;
	UBRRL = 8;
#else
#error "Unhandled F_CPU value"
#endif
	
	// Configure USART for receive & transmit, with RXC and USR interrupts.
	// Data length: 8 bits. Stop bits: 2. Parity: even.
	UCSRA = 0;
	UCSRB = (1 << RXCIE) | (1 << RXEN) | (1 << TXEN) | (1 << UDRIE);
	UCSRC = (1 << URSEL) | (1 << UPM1) | (1 << USBS) | (1 << UCSZ0) | (1 << UCSZ1);
}

bool usart_indirect_complete(void) {
	return usart_indirect_state == usart_state_unused;
}

void usart_next_frame(void) {
	cli();
	assert(USART_DIRECT_BUFFER_EMPTY());
	USART_DIRECT_BUFFER_FREE();
	sei();
}

void usart_direct_putc(char c) {
	assert(usart_indirect_complete());
	if (usart_udr_empty()) {
		UDR = c;
		return;
	}
	assert(USART_DIRECT_BUFFER_LENGTH() >= 1);
	usart_direct_buffer[usart_direct_buffer_pos++] = c;
}

#define USART_DIRECT_PUTS_BODY {								\
	if (!s) {													\
		return;													\
	}															\
																\
	assert(usart_indirect_complete());							\
	if (usart_udr_empty()) {									\
		UDR = *s++;												\
	}															\
																\
	char c;														\
	while ((c = *s++)) {										\
		assert(USART_DIRECT_BUFFER_LENGTH() >= 1);				\
		usart_direct_buffer[usart_direct_buffer_pos++] = c;		\
	}															\
}

#define USART_DIRECT_PUTS_S_BODY {								\
	if (!size) {												\
		return;													\
	}															\
																\
	assert(usart_indirect_complete());							\
	if (usart_udr_empty()) {									\
		UDR = *s++;												\
		--size;													\
	}															\
	assert(USART_DIRECT_BUFFER_LENGTH() >= size);				\
																\
	while (size--) {											\
		usart_direct_buffer[usart_direct_buffer_pos++] = *s++;	\
	}															\
}

void usart_direct_puts(const char* s)
	USART_DIRECT_PUTS_BODY

void usart_direct_putCs(const __flash char* s)
	USART_DIRECT_PUTS_BODY

void usart_direct_puts_s(const char* s, uint8_t size)
	USART_DIRECT_PUTS_S_BODY

void usart_direct_putCs_s(const __flash char* s, uint8_t size)
	USART_DIRECT_PUTS_S_BODY

void usart_indirect_puts(const char* s) {
	usart_indirect_puts_s(s, strlen(s));
}

void usart_indirect_putCs(const __flash char* s) {
	usart_indirect_putCs_s(s, strlenC(s));
}

void usart_indirect_putCs_s(const __flash char* s, uint16_t size) {
	usart_indirect_puts_s((const char*)s, size | USART_PTR_FLASH);
}

void usart_indirect_puts_s(const char* s, uint16_t size) {
	usart_indirect* in = &usart_indirect_buffer[usart_indirect_buffer_pos];
	in->ptr = s;
	in->size = size;
	USART_INDIRECT_BUFFER_NEXT(usart_indirect_buffer_pos);
	
	cli();
	if (usart_indirect_state == usart_state_unused) {
		if (USART_DIRECT_BUFFER_EMPTY()) {
			USART_DIRECT_BUFFER_FREE();
			usart_indirect_state = usart_state_busy;
			usart_populate_direct_buffer();
		} else {
			usart_indirect_state = usart_state_pending;
		}
	}
	sei();
}

ISR(USART_UDRE_vect) {
	if (USART_DIRECT_BUFFER_EMPTY()) {
		USART_DIRECT_BUFFER_FREE();
		switch (usart_indirect_state) {
		case usart_state_unused:
			return;
		case usart_state_pending:
			usart_indirect_state = usart_state_busy;
			//NOTE Fall trough
		case usart_state_busy:
			break;
		}
		if (!usart_populate_direct_buffer()) {
			usart_indirect_state = usart_state_unused;
			return;
		}
	}
	
	UDR = usart_direct_buffer[usart_direct_buffer_apos++];
}
