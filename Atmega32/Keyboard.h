/*
 * Keyboard.h - An interface to read rectangular keyboards
 *
 * Created: 2013-04-16 17:03:47
 *  Author: Gustaw Smolarczyk
 */

#ifndef Keyboard_h
#define Keyboard_h

// The size of keyboard is 4x4.
//NOTE Don't change without first consulting all the code connected to keyboard.
#define KEYBOARD_ROWS 4
#define KEYBOARD_COLUMNS 4

// Which port is connected to rows (lower 4) and columns (higher 4).
#define KEYBOARD_PORT C

// Currently clicked buttons.
extern unsigned keyboard_buttons;

// Configure port C for keyboard.
extern void keyboard_configure(void);

// Read the keyboard.
extern void keyboard_read(void);

#endif /* Keyboard_h */
