#include "Screen.h"

#include "DPad.h"
#include "Level.h"

uint32_t screen_frame = 0;
int16_t screen_offset[2] = {0, 0};

// Last offset.
int16_t screen_last_offset[2] = {0, 0};

static const __flash uint8_t patterns[] = {
#include "Patterns.bin.0.2048.bin"
};

static void screen_upload_patterns(void) {
	gterminal_write_indirectC(0, patterns, sizeof patterns);
	while (!usart_indirect_complete());
}

void screen_init(void) {
	gterminal_configure();
	gterminal_reset();
	
	gterminal_reg8_write(TERMINAL_R8_BGCOLOR, 0);
	
	gterminal_reg16_write(TERMINAL_R16_PATTERN_ADDR, SCREEN_PATTERN_ADDR);
	gterminal_reg16_write(TERMINAL_R16_MAP_ADDR, SCREEN_MAP_ADDR);
	gterminal_reg16_write(TERMINAL_R16_MAP_STRIDE, LEVEL_WIDTH);
	gterminal_reg16_write(TERMINAL_R16_MAP_LINES, LEVEL_HEIGHT);
	gterminal_reg16_write(TERMINAL_R16_RELATIVE_ADDR, SCREEN_MAP_ADDR);
	gterminal_reg16_write(TERMINAL_R16_OFFSET_X, 0);
	gterminal_reg16_write(TERMINAL_R16_OFFSET_Y, 0);
	gterminal_reg16_write(TERMINAL_R16_DIR0_STRIDE, 1);
	gterminal_reg16_write(TERMINAL_R16_DIR1_STRIDE, LEVEL_WIDTH);
	
	screen_upload_patterns();
	
	screen_enable();
}

void screen_enable(void) {
	gterminal_reg16_write(TERMINAL_R16_CONFIGURATION,
						  (1 << TERMINAL_CONF_ENABLE));
}

void screen_disable(void) {
	gterminal_reg16_write(TERMINAL_R16_CONFIGURATION, 0);
}

void screen_next_frame(void) {
	uint8_t i;
	for (i = 0; i < 2; ++i) {
		if (screen_last_offset[i] != screen_offset[i]) {
			gterminal_reg16_write(TERMINAL_R16_OFFSET_X+i, screen_offset[i]);
		}
		screen_last_offset[i] = screen_offset[i];
	}
	
	gterminal_wait_for_vsync();
	dpad_next_frame();
	++screen_frame;
}
