#include "DPad.h"

#include "Keyboard.h"

uint8_t dpad_keys = 0, dpad_previous_keys = 0;

void dpad_configure(void) {
	keyboard_configure();
}

void dpad_next_frame(void) {
	// Save previous frame keys.
	dpad_previous_keys = dpad_keys;
	
	keyboard_read();
	
	// Check individual keys.
	// The map is:
	// X ^ X X
	// < v > X
	// X X X X
	// R X X X
	// Legend:
	//  ^<v> - direction keys
	//  R - return
	//  X - unused
	
	dpad_keys = 0;
	if (keyboard_buttons & (1 << 11)) {
		dpad_keys |= (1 << DPAD_UP);
	}
	if (keyboard_buttons & (1 << 10)) {
		dpad_keys |= (1 << DPAD_DOWN);
	}
	if (keyboard_buttons & (1 << 14)) {
		dpad_keys |= (1 << DPAD_LEFT);
	}
	if (keyboard_buttons & (1 << 6)) {
		dpad_keys |= (1 << DPAD_RIGHT);
	}
	if (keyboard_buttons & (1 << 12)) {
		dpad_keys |= (1 << DPAD_RETURN);
	}
}
