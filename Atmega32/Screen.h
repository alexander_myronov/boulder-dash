/*
 * Screen.h - gterminal handler
 *
 * Created: 2013-06-19 22:36:19
 *  Author: Gustaw Smolarczyk
 */

#ifndef Screen_h
#define Screen_h

#include "Global.h"
#include "GTerminal.h"

#define SCREEN_PATTERN_ADDR 0u
#define SCREEN_MAP_ADDR (60u*1024)

// The number of frame.
extern uint32_t screen_frame;

// Offset x/y (pixel accurate)
extern int16_t screen_offset[2];

// Init screen along with gterminal.
extern void screen_init(void);

// Enable/Disable screen.
extern void screen_enable(void);
extern void screen_disable(void);

// Wait for next frame.
extern void screen_next_frame(void);

#endif /* Screen_h */
