#include "Level.h"

#include "BoulderDash.h"
#include "Screen.h"
	
static const __flash uint8_t level0[] = {
#include "Level0.bin.0.880v2.bin"
};

typedef struct {
	const __flash uint8_t* ptr;
	uint16_t needed_diamonds;
	uint8_t player_position[2];
} Level;

static const __flash Level levels[] = {
	{level0, 15, {3, 2}}
};

static void memcpyC(uint8_t* dest, const __flash uint8_t* src, uint16_t size) {
	while (size--) {
		*dest++ = *src++;
	}
}

const static uint8_t level_tile_to_real_tile[] = {
	real_tile_empty,
	real_tile_ground,
	real_tile_wall,
	real_tile_stone,
	real_tile_diamond,
	real_tile_stone,
	real_tile_diamond,
	real_tile_player,
	real_tile_exit,
	real_tile_steel,
	real_tile_butterfly,
	real_tile_butterfly,
	real_tile_butterfly,
	real_tile_butterfly,
	real_tile_firefly,
	real_tile_firefly,
	real_tile_firefly,
	real_tile_firefly,
	real_tile_explosion,
	real_tile_explosion,
	real_tile_explosion,
	real_tile_explosion,
	real_tile_explosion
};

uint8_t level_number = 0xFF;
uint8_t level_tiles[LEVEL_SIZE] = {0};
uint16_t level_needed_diamonds = 0;

void level_write(uint16_t n, uint8_t tile) {
	assert(n < LEVEL_SIZE);
	
	level_tiles[n] = tile;
	tile = level_tile_to_real_tile[tile];
	gterminal_write_rel(n, 0, &tile, 1);
}

void level_write_n(uint16_t n, uint8_t* tiles, uint8_t count,
				   uint8_t direction) {
	
	const uint16_t stride = (direction ? LEVEL_WIDTH : 1);
	uint8_t i;
	assert(n < LEVEL_SIZE);
	assert(n + count*stride <= LEVEL_SIZE);
	
	for (i = 0; i < count; ++i) {
		level_tiles[n + i*stride] = tiles[i];
		tiles[i] = level_tile_to_real_tile[tiles[i]];
	}
	gterminal_write_rel(n, direction, tiles, count);
}

void level_swap(uint16_t n, uint8_t direction) {
	const uint16_t stride = (direction ? LEVEL_WIDTH : 1);
	assert(n < LEVEL_SIZE);
	assert(n + stride <= LEVEL_SIZE);
	
	{
		const uint8_t temp = level_tiles[n];
		level_tiles[n] = level_tiles[n+stride];
		level_tiles[n+stride] = temp;
	}
	gterminal_swap_rel(n, direction);
}

void level_load(uint8_t number) {
	uint16_t i;
	int16_t center[2];
	
	assert(number < LEVEL_COUNT);
	
	screen_disable();
	
	level_number = number;
	level_needed_diamonds = levels[number].needed_diamonds;
	boulder_dash_context.player.position[0] = levels[0].player_position[0];
	boulder_dash_context.player.position[1] = levels[0].player_position[1];
	boulder_dash_context.player.state = player_alive;
	boulder_dash_context.player.diamonds = 0;
	boulder_dash_context.player.time = 120;
	
	player_load_camera(center);
	player_set_camera(center);
	
	for (i = 0; i < LEVEL_SIZE; ++i) {
		level_tiles[i] = level_tile_to_real_tile[levels[number].ptr[i]];
	}
	gterminal_write_indirect(SCREEN_MAP_ADDR, level_tiles, LEVEL_SIZE);
	
	memcpyC(level_tiles, levels[number].ptr, sizeof level_tiles);
	
	while (!usart_indirect_complete());
	//_delay_ms(1000);
	screen_enable();
}
