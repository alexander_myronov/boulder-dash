/*
 * Level.h - functions to control current level
 *
 * Created: 2013-06-20 13:18:07
 *  Author: Gustaw Smolarczyk
 */

#ifndef Level_h
#define Level_h

#include "Global.h"

#include "GTerminal.h"

// Level dimensions.
#define LEVEL_WIDTH 40
#define LEVEL_HEIGHT 22
#define LEVEL_SIZE (LEVEL_WIDTH*LEVEL_HEIGHT)

// Level count
#define LEVEL_COUNT 1

// Enumeration of all tiles.
typedef enum {
	tile_empty = 0,
	tile_ground,
	tile_wall,
	tile_stone,
	tile_diamond,
	tile_falling_stone,
	tile_falling_diamond,
	tile_player,
	tile_exit,
	tile_steel,
	tile_butterfly_north,
	tile_butterfly_east,
	tile_butterfly_south,
	tile_butterfly_west,
	tile_firefly_north,
	tile_firefly_east,
	tile_firefly_south,
	tile_firefly_west,
	tile_explosion,
	tile_explosion_center,
	tile_explosion_center2,
	tile_diamond_explosion_center,
	tile_diamond_explosion_center2
} level_tile;

typedef enum {
    real_tile_empty = 0,
	real_tile_ground,
	real_tile_wall,
	real_tile_stone,
	real_tile_diamond,
	real_tile_player,
	real_tile_exit,
	real_tile_steel,
	real_tile_butterfly,
	real_tile_firefly,
	real_tile_explosion,
} level_real_tile;

#define GAME_FALLING_DIST (tile_falling_stone - tile_stone)

// Curent level number;
extern uint8_t level_number;

// Current level tiles.
extern uint8_t level_tiles[LEVEL_SIZE];

// Current level needed diamonds.
extern uint16_t level_needed_diamonds;

static inline uint8_t level_read_xy(uint8_t x, uint8_t y) {
	assert(x < LEVEL_WIDTH);
	assert(y < LEVEL_HEIGHT);
	
	return level_tiles[x + y * LEVEL_WIDTH];
}

static inline void level_write_local_xy(uint8_t x, uint8_t y,uint8_t value) {
	assert(x < LEVEL_WIDTH);
	assert(y < LEVEL_HEIGHT);
	
	level_tiles[x + y * LEVEL_WIDTH]=value;
}

// Write both to local buffer and issue commands to
// write to GTerminal.
extern void level_write(uint16_t n, uint8_t tile);
extern void level_write_n(uint16_t n, uint8_t* tiles, uint8_t count,
						  uint8_t direction);
extern void level_swap(uint16_t n, uint8_t direction);

static inline void level_write_xy(uint8_t x, uint8_t y, uint8_t tile) {
	assert(x < LEVEL_WIDTH);
	assert(y < LEVEL_HEIGHT);
	
	level_write(x + y * LEVEL_WIDTH, tile);
}

static inline void level_write_n_xy(uint8_t x, uint8_t y, uint8_t* tiles,
									uint8_t count, uint8_t direction) {
	assert(x < LEVEL_WIDTH);
	assert(y < LEVEL_HEIGHT);
	
	level_write_n(x + y * LEVEL_WIDTH, tiles, count, direction);
}

static inline void level_swap_xy(uint8_t x, uint8_t y, uint8_t direction) {
	assert(x < LEVEL_WIDTH);
	assert(y < LEVEL_HEIGHT);
	
	level_swap(x + y * LEVEL_WIDTH, direction);
}

// Load and initialize level.
extern void level_load(uint8_t number);

#endif /* Level_h */
