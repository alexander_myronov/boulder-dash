#include "Keyboard.h"

#include "Global.h"

#include <limits.h>
#include <avr/io.h>

#define PORTX _CONCAT(PORT, KEYBOARD_PORT)
#define PINX _CONCAT(PIN, KEYBOARD_PORT)
#define DDRX _CONCAT(DDR, KEYBOARD_PORT)

unsigned keyboard_buttons = 0;

// Choose the version.
// Version2 should work better.
#define KEYBOARD_VERSION2 1

#if !KEYBOARD_VERSION2
void keyboard_configure(void) {
	STATIC_ASSERT(sizeof(keyboard_buttons) * CHAR_BIT >= KEYBOARD_ROWS * KEYBOARD_COLUMNS);
	
	// (0:3): Select row (active 0).
	// (4:7): Retrieve row (active 0), pull-up.
	DDRX = 0x0F;
	PORTX = 0xFF;
}
#else
void keyboard_configure(void) {
	STATIC_ASSERT(sizeof(keyboard_buttons) * CHAR_BIT >= KEYBOARD_ROWS * KEYBOARD_COLUMNS);
	
	// (0:3): Select row (active 0).
	// (4:7): Retrieve row (active 0), pull-up.
	DDRX = 0x00;
	PORTX = 0xF0;
}
#endif

#if !KEYBOARD_VERSION2
void keyboard_read(void) {
	keyboard_buttons = 0;
	unsigned char row_mask = 0x8;
	for (unsigned char row = 0; row < 4; ++row) {
		keyboard_buttons <<= 4;
		PORTX = ~row_mask;
		
		_delay_us(10);
		
		unsigned char row_bits = PINX;
		row_bits = ((unsigned char)~row_bits & 0xF0);
		((unsigned char*)&keyboard_buttons)[0] |= (unsigned char)(row_bits >> 4);
		row_mask >>= 1;
	}
	PORTX = 0xFF;
}
#else
void keyboard_read(void) {
	keyboard_buttons = 0;
	unsigned char row_mask = 0x8;
	for (unsigned char row = 0; row < 4; ++row) {
		keyboard_buttons <<= 4;
		DDRX = row_mask;
		
		_delay_us(10);
		
		unsigned char row_bits = PINX;
		row_bits = ((unsigned char)~row_bits & 0xF0);
		((unsigned char*)&keyboard_buttons)[0] |= (unsigned char)(row_bits >> 4);
		row_mask >>= 1;
	}
	DDRX = 0x00;
}
#endif
