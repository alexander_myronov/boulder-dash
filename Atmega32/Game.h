/*
 * Game.h - Main game logic.
 *
 * Created: 2013-06-17 20:03:55
 *  Author: Alexander Myronov
 */

#ifndef Game_h
#define Game_h

#include "Global.h"
#include "Level.h"

static inline bool game_is_firefly(uint8_t tile)
{
	return (tile >= tile_firefly_north && tile <= tile_firefly_west) 
	|| (tile >= 100+tile_firefly_north && tile <= 100+tile_firefly_west);
}

static inline bool game_is_butterfly(uint8_t tile)
{
	return (tile >= tile_butterfly_north && tile <= tile_butterfly_west) 
	|| (tile >= 100+tile_butterfly_north && tile <= 100+tile_butterfly_west);;
}

typedef enum {
	dir_north = 0,
	dir_east,
	dir_south,
	dir_west
} game_direction;

// Controls loop, fire with proper direction.
extern void game_controls_loop(game_direction dir);

// Physics loop.
extern void game_physics_loop(void);

#endif /* Game_h */
