/*
 * Led.h - An interface for 7-segment display
 *
 * Created: 2013-04-16 16:52:51
 *  Author: Gustaw Smolarczyk
 */

#ifndef Led_h
#define Led_h

#include "Global.h"

// 0..15 are hexadecimal digits
#define LED_EMPTY 16
#define LED_UP 17
#define LED_DOWN 18

// The number of digits on a display (max 8).
#define LED_DIGITS_COUNT 4

// Change displayed digit every 5ms.
#define LED_CHANGE_DELAY 5

// Which port is connected to 7+1 segments.
#define LED_SEGMENTS_PORT A

// Which port is connected to selecting transistors.
#define LED_SELECT_PORT B

// A BCD number to display.
extern unsigned char led_digits[LED_DIGITS_COUNT];

// Which digit is currently lit.
extern unsigned char led_digit_index;

// Configure ports A and B for LED.
extern void led_configure(void);

// Change the displayed digit.
extern void led_change_digit(void);

// An utility function to convert an unsigned number to hex digits.
// Returns 1 when number is too big, 0 otherwise.
extern char led_hex_to_bcd(unsigned number, unsigned char bcd[LED_DIGITS_COUNT]);

// An utility function to convert an unsigned number to dec digits.
// Returns 1 when number is too big, 0 otherwise.
extern char led_dec_to_bcd(unsigned number, unsigned char bcd[LED_DIGITS_COUNT]);

extern uint16_t secondTimer;
extern uint16_t second;

#endif /* Led_h */
