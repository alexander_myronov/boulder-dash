#include "GTerminal.h"

void gterminal_configure(void) {
	usart_configure();
}

#ifndef BD_MOCK
static volatile bool vsyncFlag = false;

void gterminal_wait_for_vsync(void) {
	vsyncFlag = false;
	while (!vsyncFlag);
	vsyncFlag = false;
	usart_next_frame();
}

USART_RECEIVE_INTERRUPT() {
	char c = UDR;
	gterminal_divisor_callback(c);
	if (c == 0) {
		vsyncFlag = true;
	}
}
#endif

static uint8_t gterminal_buffer[5];

void gterminal_reg8_write(uint8_t reg, uint8_t data) {
	assert(reg < TERMINAL_R8_NUM);
	
	gterminal_buffer[0] = TERMINAL_REG8_WRITE | reg;
	gterminal_buffer[1] = data;
	usart_direct_puts_s((const char*)gterminal_buffer, TERMINAL_REG8_WRITE_LENGTH);
}

void gterminal_reg16_write(uint8_t reg, uint16_t data) {
	assert(reg < TERMINAL_R16_NUM);
	
	gterminal_buffer[0] = TERMINAL_REG16_WRITE | reg;
	gterminal_buffer[1] = (data & 0xFF);
	gterminal_buffer[2] = (data >> 8);
	usart_direct_puts_s((const char*)gterminal_buffer, TERMINAL_REG16_WRITE_LENGTH);
}

void gterminal_write(uint16_t addr, const uint8_t* data, uint8_t dataSize) {
	uint8_t pos = 0;

	if (_UNLIKELY(dataSize-- == 0)) {
        return;
    }

	if (!(dataSize & ~TERMINAL_WRITE_ABS_SIZEMASK)) {
		gterminal_buffer[pos++] = TERMINAL_WRITE_ABS | dataSize;
	} else {
		gterminal_buffer[pos++] = TERMINAL_WRITE_ABSS;
		gterminal_buffer[pos++] = dataSize;
	}
	
	gterminal_buffer[pos++] = (addr & 0xFF);
	gterminal_buffer[pos++] = (addr >> 8);
	usart_direct_puts_s((const char*)gterminal_buffer, pos);
	usart_direct_puts_s((const char*)data, dataSize+1);
}

void gterminal_write_rel(uint16_t addr, uint8_t direction, const uint8_t* data, uint8_t dataSize) {
	uint8_t pos = 0;
	
	assert(!(addr & ~TERMINAL_WRITE_REL_ADDRMASK));
	assert((direction & 1) == direction);

	if (_UNLIKELY(dataSize-- == 0)) {
        return;
    }

	if (_LIKELY(!(dataSize & ~TERMINAL_WRITE_REL_SIZEMASK))) {
		gterminal_buffer[pos++] = TERMINAL_WRITE_REL |
						          (dataSize << 4) |
                                  (direction << 3) |
						          (addr >> 8);
	} else {
		gterminal_buffer[pos++] = TERMINAL_WRITE_RELS |
                                  (direction << 3) |
						          (addr >> 8);
		gterminal_buffer[pos++] = dataSize;
	}
	
	gterminal_buffer[pos++] = (addr & 0xFF);
	usart_direct_puts_s((const char*)gterminal_buffer, pos);
	usart_direct_puts_s((const char*)data, dataSize+1);
}

void gterminal_swap_rel(uint16_t addr, uint8_t direction) {
	assert(!(addr & ~TERMINAL_WRITE_SWAP_ADDRMASK));
	assert((direction & 1) == direction);

	gterminal_buffer[0] = TERMINAL_WRITE_SWAP |
				(direction << 3) |
				(addr >> 8);
	gterminal_buffer[1] = (addr & 0xFF);
	usart_direct_puts_s((const char*)gterminal_buffer, TERMINAL_WRITE_SWAP_LENGTH);
}

void gterminal_reset(void) {
	usart_direct_putc(TERMINAL_RESET);
}

void gterminal_write_big_begin(uint16_t addr, uint16_t dataSize) {
	gterminal_buffer[0] = TERMINAL_WRITE_ABSB;
	gterminal_buffer[1] = ((dataSize-1) & 0xFF);
	gterminal_buffer[2] = ((dataSize-1) >> 8);
	gterminal_buffer[3] = (addr & 0xFF);
	gterminal_buffer[4] = (addr >> 8);
	
	usart_direct_puts_s((const char*)gterminal_buffer, TERMINAL_WRITE_ABSB_LENGTH-1);
}

void gterminal_write_indirect(uint16_t addr, const uint8_t* data, uint16_t dataSize) {
	gterminal_write_big_begin(addr, dataSize);
	usart_indirect_puts_s((const char*)data, dataSize);
}

void gterminal_write_indirectC(uint16_t addr, const __flash uint8_t* data, uint16_t dataSize) {
	gterminal_write_big_begin(addr, dataSize);
	usart_indirect_putCs_s((const __flash char*)data, dataSize);
}
