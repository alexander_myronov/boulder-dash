#include "HD44780.h"

#include <avr/io.h>

#ifndef HD44780_PORT
#error "HD44780_PORT should be defined to the port connected to HD44780 device."
#endif

#define PORTX _CONCAT(PORT, HD44780_PORT)
#define DDRX _CONCAT(DDR, HD44780_PORT)

static inline void hd44780_write(uint8_t data, uint8_t rs) {
	uint8_t bits = data & 0xF0;
	PORTX = (rs << HD44780_RS) | (PORTX & ~HD44780_MASK);
	_delay_us(0.040);
	PORTX |= (1 << HD44780_E) | bits;
	_delay_us(0.230);
	PORTX &= ~(1 << HD44780_E);
	_delay_us(0.270);
	bits = data << 4;
	PORTX = (rs << HD44780_RS) | (1 << HD44780_E) | bits | (PORTX & ~HD44780_MASK);
	_delay_us(0.230);
	PORTX &= ~(1 << HD44780_E);
	_delay_us(0.270);
}

static inline void hd44780_write_instruction(uint8_t cmd) {
	hd44780_write(cmd, 0);
}

void hd44780_configure(uint8_t lines, uint8_t font) {
	DDRX |= HD44780_MASK;
	// Set the port so that no command will execute for now.
	PORTX = (PORTX & ~HD44780_MASK);
	
	// Wait for at least 15/40ms (depends on voltage).
	_delay_ms(50);
	
	// Issue SET_FUNCTION instruction with DL = 1 three times.
	{
		const uint8_t cmd = ((0b0011) << HD44780_DH);
		PORTX |= (1 << HD44780_E) | cmd;
		_delay_us(0.230);
		PORTX = cmd | (PORTX & ~HD44780_MASK);
		
		// Wait for at least 4.1ms.
		_delay_ms(5);
	}
	{
		const uint8_t cmd = ((0b0011) << HD44780_DH);
		PORTX |= (1 << HD44780_E) | cmd;
		_delay_us(0.230);
		PORTX = cmd | (PORTX & ~HD44780_MASK);
		
		// Wait for at least 100us.
		_delay_us(200);
	}
	{
		const uint8_t cmd = ((0b0011) << HD44780_DH);
		PORTX |= (1 << HD44780_E) | cmd;
		_delay_us(0.230);
		PORTX = cmd | (PORTX & ~HD44780_MASK);
		_delay_us(0.270);
		
		// Normal delay.
		HD44780_DELAY(HD44780_DELAY_NORMAL);
	}
	
	// Set DL to 0.
	{
		const uint8_t cmd = ((0b0010) << HD44780_DH);
		PORTX |= (1 << HD44780_E) | cmd;
		_delay_us(0.230);
		PORTX = cmd | (PORTX & ~HD44780_MASK);
		_delay_us(0.270);
		HD44780_DELAY(HD44780_DELAY_NORMAL);
	}

	// Now we can do proper configuration.
	hd44780_set_function_sync(lines, font);
	hd44780_display_control_sync(false, false, false);
	hd44780_clear_display_sync();
	hd44780_set_entry_mode_sync(HD44780_SET_ENTRY_MODE_DIRECTION_INCREMENT, false);
	//hd44780_setDDRAMAddress(0);
}

void hd44780_clear_display(void) {
	hd44780_write_instruction(HD44780_CLEAR_DISPLAY);
}

void hd44780_return_home(void) {
	hd44780_write_instruction(HD44780_RETURN_HOME);
}

void hd44780_set_entry_mode(uint8_t direction, bool shift) {
	hd44780_write_instruction(HD44780_SET_ENTRY_MODE |
		(direction << HD44780_SET_ENTRY_MODE_DIRECTION) |
		(shift << HD44780_SET_ENTRY_MODE_SHIFT));
}

void hd44780_display_control(bool display, bool cursor, bool blink) {
	hd44780_write_instruction(HD44780_DISPLAY_CONTROL |
		(display << HD44780_DISPLAY_CONTROL_DISPLAY) |
		(cursor << HD44780_DISPLAY_CONTROL_CURSOR) |
		(blink << HD44780_DISPLAY_CONTROL_BLINK));
}

void hd44780_cursor_display_shift(bool display, uint8_t direction) {
	hd44780_write_instruction(HD44780_CURSOR_DISPLAY_SHIFT |
		(display << HD44780_CURSOR_DISPLAY_SHIFT_DISPLAY) |
		(direction << HD44780_CURSOR_DISPLAY_SHIFT_DIRECTION));
}

void hd44780_set_function(uint8_t lines, uint8_t font) {
	hd44780_write_instruction(HD44780_SET_FUNCTION |
		(lines << HD44780_SET_FUNCTION_LINES) |
		(font << HD44780_SET_FUNCTION_FONT));
}

void hd44780_set_cgram_address(uint8_t address) {
	hd44780_write_instruction(HD44780_SET_CGRAM_ADDRESS | address);
}

void hd44780_set_ddram_address(uint8_t address) {
	hd44780_write_instruction(HD44780_SET_DDRAM_ADDRESS | address);
}

void hd44780_goto(uint8_t x, uint8_t y) {
	hd44780_set_ddram_address(x + y * 0x40);
}

void hd44780_write_data(uint8_t data) {
	hd44780_write(data, 1);
}

void hd44780_puts(char* str) {
	while (*str) {
		hd44780_write_data_sync(*str);
		++str;
	}
}
