#include "Led.h"

#include <string.h>
#include <avr/io.h>

#ifndef LED_SEGMENTS_PORT
#error "LED_SEGMENTS_PORT should be defined to the port connected to 7+1 segments."
#endif

#ifndef LED_SELECT_PORT
#error "LED_SELECT_PORT should be defined to the port connected to transistors."
#endif

#define PORT_7 _CONCAT(PORT, LED_SEGMENTS_PORT)
#define DDR_7 _CONCAT(DDR, LED_SEGMENTS_PORT)

#define PORT_S _CONCAT(PORT, LED_SELECT_PORT)
#define DDR_S _CONCAT(DDR, LED_SELECT_PORT)

// LUT to translate a hex digit (0..F) to proper
// 7-segment+dp mask.
static const unsigned char led_digit_to_7[19] = {
	0b11000000, // 0
	0b11111001, // 1
	0b10100100, // 2
	0b10110000, // 3
	0b10011001, // 4
	0b10010010, // 5
	0b10000010, // 6
	0b11111000, // 7
	0b10000000, // 8
	0b10010000, // 9
	0b10001000, // A
	0b10000011, // B
	0b11000110, // C
	0b10100001, // D
	0b10000110, // E
	0b10001110, // F
	0b11111111, // Empty
	0b11001000, // Up
	0b10101011  // Down
};

unsigned char led_digits[LED_DIGITS_COUNT] = {
	LED_EMPTY, LED_EMPTY, LED_EMPTY, LED_EMPTY
};

unsigned char led_digit_index = LED_DIGITS_COUNT-1;

void led_configure(void) {
	// A: Which segments are lit (active 0).
	DDR_7 = 0xFF;
	PORT_7 = 0xFF;
	// B: Which columns are lit (active 0).
	DDR_S = 0xFFu >> (8-LED_DIGITS_COUNT);
	PORT_S = DDR_S;

	TCCR0 |= (1 << WGM01);
	TCCR0 &= ~(1 << WGM00);
	OCR0 = 250;
	TIMSK &= ~(1 << TOIE0);
	TIMSK |= (1 << OCIE0);
	
	TCCR0 |= (1 << CS00) | (1 << CS01);
}

static uint8_t count = 5;
uint16_t secondTimer = 0;
uint16_t second = 0;

ISR(TIMER0_COMP_vect) {
	if (--count == 0) {
		count = 5;
		led_change_digit();
	}
	if (++secondTimer == 1000) {
		secondTimer = 0;
		++second;
	}
}

void led_change_digit(void) {
	// Turn off current digit.
	PORT_7 = 0xFF;
	
	// Choose next digit.
	led_digit_index = (led_digit_index+1) % LED_DIGITS_COUNT;
	PORT_S = ~(1 << led_digit_index);
	
	unsigned char mask = led_digit_to_7[led_digits[led_digit_index]];
	PORT_7 = mask;
}

char led_hex_to_bcd(unsigned number, unsigned char bcd[LED_DIGITS_COUNT]) {
	memset(bcd, LED_EMPTY, LED_DIGITS_COUNT);
	if (number == 0) {
		bcd[0] = 0;
		return 0;
	}
	for (unsigned char i = 0; i < LED_DIGITS_COUNT; ++i) {
		bcd[i] = number & 0xF;
		number >>= 4;
		if (number == 0) {
			return 0;
		}
	}
	
	return 1;
}

char led_dec_to_bcd(unsigned number, unsigned char bcd[LED_DIGITS_COUNT]) {
	memset(bcd, LED_EMPTY, LED_DIGITS_COUNT);
	if (number == 0) {
		bcd[0] = 0;
		return 0;
	}
	for (unsigned char i = 0; i < LED_DIGITS_COUNT; ++i) {
		bcd[i] = number % 10;
		number /= 10;
		if (number == 0) {
			return 0;
		}
	}
	
	return 1;
}
