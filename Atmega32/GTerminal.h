/*
 * GTerminal.h - A driver for GTerminal (using USART)
 *
 * Created: 2013-06-11 16:17:54
 *  Author: Gustaw Smolarczyk
 */

#ifndef GTerminal_h
#define GTerminal_h

#include "Global.h"
#include "USART.h"

#include "Terminal.h"

#define GTERMINAL_PATTERN_WIDTH 16
#define GTERMINAL_PATTERN_HEIGHT 16
#define GTERMINAL_WINDOW_WIDTH 320
#define GTERMINAL_WINDOW_HEIGHT 240

// Configure the terminal, along with USART.
extern void gterminal_configure(void);

// Wait for vsync.
extern void gterminal_wait_for_vsync(void);

//NOTE These should not be used for continous transfer.
// I.e. make sure that usart direct buffer is empty after vsync
// before you use them.
extern void gterminal_reset(void);
extern void gterminal_reg8_write(uint8_t reg, uint8_t data);
extern void gterminal_reg16_write(uint8_t reg, uint16_t data);
extern void gterminal_write(uint16_t addr, const uint8_t* data, uint8_t dataSize);
extern void gterminal_write_rel(uint16_t addr, uint8_t direction, const uint8_t* data, uint8_t dataSize);
extern void gterminal_swap_rel(uint16_t addr, uint8_t direction);

// Write using indirect buffer. Only one pending write is possible.
// Wait using usart_indirect_complete.
extern void gterminal_write_indirect(uint16_t addr, const uint8_t* data, uint16_t dataSize);
extern void gterminal_write_indirectC(uint16_t addr, const __flash uint8_t* data, uint16_t dataSize);

// Begin big transfer.
extern void gterminal_write_big_begin(uint16_t addr, uint16_t dataSize);

// Define divisor callback with GTERMINAL_DIVISOR_CALLBACK(argName) { ... }
extern void gterminal_divisor_callback(uint8_t arg);
#define GTERMINAL_DIVISOR_CALLBACK(arg) void gterminal_divisor_callback(uint8_t arg)

#endif /* GTerminal_h */
