#include "BoulderDash.h"
#include "DPad.h"
#include "Game.h"
#include "HD44780.h"
#include "Led.h"
#include "Level.h"
#include "Screen.h"

#include <string.h>

boulder_dash_context_t boulder_dash_context;

GTERMINAL_DIVISOR_CALLBACK(count) {
	(void)count;
	//led_change_digit();
}

bool player_exit_visible(void) {
	return boulder_dash_context.player.diamonds >= level_needed_diamonds;
}

static uint8_t player_camera_remaining = 0;

void player_load_camera(int16_t coords[2]) {
	coords[0] = boulder_dash_context.player.position[0]*GTERMINAL_PATTERN_WIDTH
		+ (GTERMINAL_PATTERN_WIDTH - GTERMINAL_WINDOW_WIDTH)/2;
	coords[1] = boulder_dash_context.player.position[1]*GTERMINAL_PATTERN_HEIGHT
		+ (GTERMINAL_PATTERN_HEIGHT - GTERMINAL_WINDOW_HEIGHT)/2;
}

void player_move_camera(void) {
	int16_t player_center[2];
	
    uint8_t step = player_camera_remaining + PLAYER_CAMERA_STEP;
	uint8_t i;
	int16_t coords[2];
	player_load_camera(player_center);

    player_camera_remaining = step % PLAYER_CAMERA_STEP_DIV;
    step /= PLAYER_CAMERA_STEP_DIV;

	
	for (i = 0; i < 2; ++i) {
		if (screen_offset[i] < player_center[i]) {
			if (player_center[i] - screen_offset[i] < step) {
				coords[i] = player_center[i];
			} else {
				coords[i] = screen_offset[i] + step;
			}
		} else if (screen_offset[i] > player_center[i]) {
			if (screen_offset[i] - player_center[i] < step) {
				coords[i] = player_center[i];
			} else {
				coords[i] = screen_offset[i] - step;
			}
		} else {
			coords[i] = screen_offset[i];
		}
	}
	
	player_set_camera(coords);
}

void player_set_camera(const int16_t coords[2]) {
	uint8_t i;
	static const int16_t max_dim[2] = {
		LEVEL_WIDTH*GTERMINAL_PATTERN_WIDTH - GTERMINAL_WINDOW_WIDTH,
		LEVEL_HEIGHT*GTERMINAL_PATTERN_HEIGHT - GTERMINAL_WINDOW_HEIGHT
	};
	
	for (i = 0; i < 2; ++i) {
		if (coords[i] >= max_dim[i]) {
			screen_offset[i] = max_dim[i];
		} else if (coords[i] < 0) {
			screen_offset[i] = 0;
		} else {
			screen_offset[i] = coords[i];
		}
	}
}

void player_move(void) {
	if (DPAD_KEY(LEFT) && !DPAD_KEY(RIGHT)) {
		game_controls_loop(dir_west);
		return;
	}
	if (!DPAD_KEY(LEFT) && DPAD_KEY(RIGHT)) {
		game_controls_loop(dir_east);
		return;
	}
	if (DPAD_KEY(UP) && !DPAD_KEY(DOWN)) {
		game_controls_loop(dir_north);
		return;
	}
	if (!DPAD_KEY(UP) && DPAD_KEY(DOWN)) {
		game_controls_loop(dir_south);
		return;
	}
}

void boulder_dash_init(void) {
	memset(&boulder_dash_context, 0, sizeof boulder_dash_context);
	
	hd44780_configure(
		HD44780_SET_FUNCTION_LINES_TWO,
		HD44780_SET_FUNCTION_FONT_5x8);
	hd44780_display_control_sync(true, false, false);
	
	sei();
	
	screen_init();
	dpad_configure();
	
	led_configure();
	
	boulder_dash_context.player.lives = 3;
	boulder_dash_context.player.score = 0;
}

static uint16_t lastSecond;

#define DPAD_COUNT 2
#define PHYSICS_COUNT 2
#define RELOAD_COUNT 3
void boulder_dash_exec(void) {
	uint8_t dpad_counter = DPAD_COUNT;
	uint8_t physics_counter = PHYSICS_COUNT;
	int8_t reload_counter = -1;
	
	level_load(0);
	led_dec_to_bcd(boulder_dash_context.player.time, led_digits);
	secondTimer = 0;
	lastSecond = second = 0;
	
	while (true) {
		screen_next_frame();
		if (second != lastSecond) {
			uint16_t time = --boulder_dash_context.player.time;
			led_dec_to_bcd(time, led_digits);
			if(time == 0)
			{
				boulder_dash_context.player.state = player_dead;
			}
			
			lastSecond = second;
		}
		if(reload_counter == -1)
		{
			if (--dpad_counter == 0) {
				player_move();
				dpad_counter = DPAD_COUNT;
			}
		}
		else
		{
			
			if(--reload_counter == 0) {
				if(boulder_dash_context.player.lives>=0)
				{
					level_load(0);
					led_dec_to_bcd(boulder_dash_context.player.time, led_digits);
					secondTimer = 0;
					lastSecond = second = 0;
					reload_counter = -1;
				}
				else
				{
					screen_disable();
					return;
				}
			}
		}
		if (--physics_counter == 0) {
			game_physics_loop();
			physics_counter = PHYSICS_COUNT;
		}
		
		
		player_move_camera();
		if(reload_counter == -1 && (boulder_dash_context.player.state == player_win || boulder_dash_context.player.state == player_dead))
		{
			reload_counter = RELOAD_COUNT;
			
		}
		
		player_show_numbers();
	}
}

void player_show_numbers(void) {
	char buffer[12];
	uint16_t score = boulder_dash_context.player.score;
	int8_t lives = boulder_dash_context.player.lives;
	int8_t i = 4;
	memset(buffer, ' ', sizeof buffer);
	buffer[sizeof buffer-1] = '\0';
	
	hd44780_goto_sync(0, 0);
	
	if (lives < 0) {
		const char gameOver[] = "Game Over";
		memcpy(buffer, gameOver, sizeof gameOver);
		memset(led_digits, LED_EMPTY, LED_DIGITS_COUNT);
		hd44780_puts(buffer);
		return;
	}
	
	buffer[4] = '0';
	
	while (score > 0) {
		buffer[i] = (score % 10) + '0';
		score /= 10;
		--i;
	}
	
	for (i = 0; i < lives; ++i) {
		buffer[6+i] = 'X';
	}
	
	hd44780_puts(buffer);
	hd44780_goto_sync(0, 1);
	memset(buffer, ' ', sizeof buffer-1);
	
	score = boulder_dash_context.player.diamonds;
	buffer[i = 3] = '0';
	while (score > 0) {
		buffer[i] = (score % 10) + '0';
		score /= 10;
		--i;
	}
	
	buffer[4] = '/';
	score = level_needed_diamonds;
	buffer[i = 7] = '0';
	while (score > 0) {
		buffer[i] = (score % 10) + '0';
		score /= 10;
		--i;
	}
	
	hd44780_puts(buffer);
}
