/*
 * USART.h - An interface for USART port
 *
 * Created: 2013-04-23 18:11:23
 *  Author: Gustaw Smolarczyk
 */

#ifndef USART_h
#define USART_h

#include "Global.h"

#define USART_DIRECT_BUFFER_SIZE 255
#define USART_INDIRECT_BUFFER_SIZE 16

// Configure USART for 9600bps & 8 bits.
// Also configure USART_RXC/UDRE interrupts.
// UDRE is already defined, RXC should be implemented by user.
extern void usart_configure(void);

// Returns true if the indirect buffer transfer is complete.
// Only then the direct buffer can be used again.
extern bool usart_indirect_complete(void);

// Asserts, that the direct buffer is empty.
extern void usart_next_frame(void);

// These functions deal with the direct buffer.
// The direct buffer only holds data for a single frame.
extern void usart_direct_putc(char c);
extern void usart_direct_puts(const char* s);
extern void usart_direct_putCs(const __flash char* s);
extern void usart_direct_puts_s(const char* s, uint8_t size);
extern void usart_direct_putCs_s(const __flash char* s, uint8_t size);

// These functions deal with the indirect buffer.
extern void usart_indirect_puts(const char* s);
extern void usart_indirect_putCs(const __flash char* s);
extern void usart_indirect_puts_s(const char* s, uint16_t size);
extern void usart_indirect_putCs_s(const __flash char* s, uint16_t size);

// Define receive interrupt with USART_RECEIVE_INTERRUPT() { ... }
#ifdef BD_MOCK
	extern void usart_receive_interrupt(void);
#	define USART_RECEIVE_INTERRUPT() void usart_receive_interrupt(void)
#else
#	define USART_RECEIVE_INTERRUPT() ISR(USART_RXC_vect)
#endif

#endif /* USART_h */
