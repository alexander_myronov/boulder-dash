/*
 * DPad.h - A wrapper for keyboard.
 *
 * Created: 2013-06-14 20:18:48
 *  Author: Gustaw Smolarczyk
 */

#ifndef DPad_h
#define DPad_h

#include "Global.h"

// Bits for actual keys.
#define DPAD_UP 0
#define DPAD_DOWN 1
#define DPAD_LEFT 2
#define DPAD_RIGHT 3
#define DPAD_RETURN 4

// A helper macro for testing keys.
#define DPAD_KEY2(x, key) (((x) >> _CONCAT(DPAD_, key)) & 1)
#define DPAD_KEY(key) DPAD_KEY2(dpad_keys, key)

// Whether the key changed.
#define DPAD_CHANGE(key) (DPAD_KEY2(dpad_keys, key) ^ DPAD_KEY2(dpad_previous_keys, key))

// Change && pushed.
#define DPAD_PUSHDOWN(key) DPAD_CHANGE(key) & DPAD_KEY2(dpad_keys, key)

// Change && not pushed.
#define DPAD_PUSHUP(key) DPAD_CHANGE(key) & (DPAD_KEY2(dpad_keys, key) ^ 1)

// Current and previous dpad keys.
extern uint8_t dpad_keys, dpad_previous_keys;

// Configure the dpad, along with keyboard.
extern void dpad_configure(void);

// Read keyboard every frame.
extern void dpad_next_frame(void);

#endif /* DPad_h */
