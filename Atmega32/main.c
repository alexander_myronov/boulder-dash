/*
 * main.c
 *
 * Created: 2013-06-11 13:09:34
 *  Author: Gustaw Smolarczyk
 */

#include "BoulderDash.h"
#include "Global.h"

int main(void) {
	boulder_dash_init();
	boulder_dash_exec();
	
	FOREVER_LOOP();
}
