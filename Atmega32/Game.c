#include "Game.h"

#include "BoulderDash.h"
#include "Level.h"

extern boulder_dash_context_t boulder_dash_context;

static const int8_t dx[] = {0,1,0,-1};
static const int8_t dy[] = {-1,0,1,0};

static inline void swap(uint8_t x, uint8_t y, game_direction dir) 
{
	level_swap_xy(dx[dir] == -1 ? x-1 : x,
				  dy[dir] == -1 ? y-1 : y,
				  dy[dir] & 1);
}

static inline void swapToEmpty(uint8_t x, uint8_t y, game_direction dir) 
{
	const uint8_t tile = level_read_xy(x, y);
	uint8_t buffer[2] = {tile_empty, tile_empty};
	buffer[(dx[dir]+dy[dir]) == -1 ? 0 : 1] = tile;
	level_write_n_xy(dx[dir] == -1 ? x-1 : x,
					 dy[dir] == -1 ? y-1 : y,
					 buffer, 2, dy[dir] & 1);
}

static inline void setToValue(uint8_t x, uint8_t y, uint8_t value) 
{
	level_write_xy(x, y, value);
}


static
void game_die()
{
	boulder_dash_context.player.lives--;
	boulder_dash_context.player.state = player_dead;
}

static
void game_win()
{
	boulder_dash_context.player.state = player_win;
}

static uint8_t abs8(int8_t n) {
	return (n < 0 ? -n : n);
}

static
bool isPlayerInRange(uint8_t x,uint8_t y)
{
	return abs8(boulder_dash_context.player.position[0] - x)<=1 && 
		abs8(boulder_dash_context.player.position[1] - y)<=1;

}

static
void game_explosion(uint8_t x,uint8_t y,bool player_check)
{
	uint8_t buffer[3] = {tile_explosion, tile_explosion,tile_explosion};
	uint8_t bufferCenter[3] = {tile_explosion, tile_explosion_center2,tile_explosion};
    uint8_t buffer2[3] = {tile_explosion, tile_explosion,tile_explosion};
	level_write_n_xy(x-1,y-1,buffer,3,0);
	level_write_n_xy(x-1,y,bufferCenter,3,0);
	level_write_n_xy(x-1,y+1,buffer2,3,0);
	if(player_check && isPlayerInRange(x,y))
		game_die();
}

static
void game_explosion_cooldown(uint8_t x,uint8_t y)
{
	uint8_t buffer[3] = {tile_empty, tile_empty,tile_empty};
    //NOTE OPTIMIZATION
	level_write_n_xy(x-1,y-1,buffer,3,0);
	level_write_n_xy(x-1,y,buffer,3,0);
	level_write_n_xy(x-1,y+1,buffer,3,0);
}

static
void game_diamond_explosion(uint8_t x,uint8_t y,bool player_check)
{
	uint8_t buffer[3] = {tile_explosion, tile_explosion,tile_explosion};
	uint8_t bufferCenter[3] = {tile_explosion, tile_diamond_explosion_center2,tile_explosion};
    uint8_t buffer2[3] = {tile_explosion, tile_explosion,tile_explosion};
	level_write_n_xy(x-1,y-1,buffer,3,0);
	level_write_n_xy(x-1,y,bufferCenter,3,0);
	level_write_n_xy(x-1,y+1,buffer2,3,0);
	if(player_check && isPlayerInRange(x,y))
		game_die();
}

static
void game_diamond_explosion_cooldown(uint8_t x,uint8_t y)
{
	uint8_t buffer[3] = {tile_diamond, tile_diamond,tile_diamond};
    uint8_t buffer2[3] = {tile_diamond, tile_diamond,tile_diamond};
    uint8_t buffer3[3] = {tile_diamond, tile_diamond,tile_diamond};
	level_write_n_xy(x-1,y-1,buffer,3,0);
	level_write_n_xy(x-1,y,buffer2,3,0);
	level_write_n_xy(x-1,y+1,buffer3,3,0);
}

static
void game_get_diamond()
{
	boulder_dash_context.player.diamonds++;
	boulder_dash_context.player.score += 20;
}

void game_controls_loop(game_direction dir) {
	const uint8_t curX = boulder_dash_context.player.position[0],
				  curY = boulder_dash_context.player.position[1];
	const uint8_t nextX = curX + dx[dir],
				  nextY = curY + dy[dir];
	
	uint8_t tile = level_read_xy(nextX, nextY);
	switch (tile) 
	{
	case tile_empty:
		swap(curX, curY, dir);
		break;
	case tile_ground:
		swapToEmpty(curX, curY, dir);
		break;
	case tile_wall:
	case tile_steel:
		return;
	case tile_falling_diamond:
	case tile_diamond:
		swapToEmpty(curX, curY, dir);
		game_get_diamond();
		break;
	case tile_stone:
	case tile_falling_stone:
		if (!(dir == dir_east || dir == dir_west)) 
		{
			return;
		} 
		else 
		{
			const uint8_t nnX = nextX + dx[dir],
						  nnY = curY;
			if (level_read_xy(nnX, nnY) != tile_empty) 
			{
				return;
			}
			
			swap(nextX, nextY, dir);
			swap(curX, curY, dir);
		}
		break;
	case tile_exit:
		if(boulder_dash_context.player.diamonds >= level_needed_diamonds)
		{
			swapToEmpty(curX, curY, dir);
			game_win();
		}
		return;
	case tile_player:
	default:
		assert(!"Impossible");
		return;
	}
	if(game_is_butterfly(tile))
	{
		game_diamond_explosion(nextX,nextY,false);
		game_die();

	}
	else if(game_is_firefly(tile))
	{
		game_explosion(nextX,nextY,false);
		game_die();

	}
	boulder_dash_context.player.position[0] = nextX;
	boulder_dash_context.player.position[1] = nextY;
}

static
bool isFallingInNeighbourColumn(int x, int y, int leftOrRight) 
{
	int y2;
	for (y2 = y; y >= 0; --y) 
	{
		uint8_t tile = level_read_xy(x, y2);
		if (tile != tile_diamond ||
			tile != tile_falling_diamond || 
			tile != tile_stone ||
			tile != tile_falling_stone) 
		{
			
			return false;
		}
		tile = level_read_xy(x + leftOrRight, y2);
		if (tile == tile_falling_diamond ||
			tile == tile_falling_stone ||
			tile == tile_player)
		{
			
			return true;
		}
	}
	return false;
}

static
bool isFallingInThisColumn(int x,int y) 
{
	int y2;
	for (y2 = y-1; y >= 0; --y) 
	{
		const uint8_t tile = level_read_xy(x, y2);
		if (tile != tile_empty ) 
		{
			return false;
		}
		if (tile == tile_falling_diamond ||
			tile == tile_falling_stone) 
		{
			
			return true;
		}
	}
	return false;
}

void game_physics_loop(void) 
{
	int8_t x, y;
	y = LEVEL_HEIGHT-2;
	
	while (y >= 0) 
	{
		for (x = 0; x < LEVEL_WIDTH; ++x) 
		{
			const uint8_t tile = level_read_xy(x, y);
			const uint8_t below = level_read_xy(x, y+1);
			
			switch (tile) 
			{
			case tile_stone:
			case tile_diamond:
				if (below == tile_empty) 
				{
					setToValue(x, y, tile + GAME_FALLING_DIST);
				} 
				else if (below == tile_falling_stone ||
						   below == tile_falling_diamond) 
				{
					// tile_* -> tile_falling_*
					setToValue(x, y, tile + GAME_FALLING_DIST);
				} 
				
				break;
			case tile_falling_stone:
			case tile_falling_diamond:
				if (below == tile_empty) {
					const uint8_t bbelow = level_read_xy(x, y+2);
					if (bbelow != tile_falling_stone &&
						bbelow != tile_falling_diamond)
					{
						
						swap(x, y, dir_south);
					}
				} 
				else if (below == tile_player) 
				{
					game_die();
					game_explosion(x,y+1,false);

				} 
				else if(game_is_butterfly(below))
				{
					game_diamond_explosion(x,y+1,true);
				}
				else if(game_is_firefly(below))
				{
					game_explosion(x,y+1,true);
				}
				else if (below == tile_falling_stone ||
						   below == tile_falling_diamond)
				{
					//DO NOTHING
				} 
				else 
				{
					// tile_falling_* -> tile_*
					setToValue(x, y, tile - GAME_FALLING_DIST);
				}
				break;
			case tile_butterfly_north:
			case tile_butterfly_east:
			case tile_butterfly_south:
			case tile_butterfly_west:
				//butterfly 
				//a) turns to the right and moves, if possible
				//b) moves forward, if possible
				//c) turns to the left otherwise
				{
					uint8_t dirOffset = tile - tile_butterfly_north;
					uint8_t dirOffsetRight = (dirOffset+1)%4;
					uint8_t rightX = x + dx[dirOffsetRight],rightY = y + dy[dirOffsetRight];
					if(level_read_xy(rightX,rightY) == tile_empty)//tile to the right
					{
						level_write_xy(x,y,tile_butterfly_north + dirOffsetRight);
						swap(x,y,(game_direction)(dirOffsetRight));
						//HACK to exclude the tile from further processing
						level_write_local_xy(rightX,rightY,tile_butterfly_north + dirOffsetRight+100);
					}
					else if(level_read_xy(rightX,rightY) == tile_player)
					{
						game_die();
						game_diamond_explosion(x,y,false);
					}
					else if(level_read_xy(x+dx[dirOffset],y+dy[dirOffset]) == tile_empty)
					{
						swap(x,y,(game_direction)(dirOffset));
						level_write_local_xy(x+dx[dirOffset],y+dy[dirOffset],tile+100);
					}
					else if(level_read_xy(x+dx[dirOffset],y+dy[dirOffset]) == tile_player)
					{
						game_die();
						game_diamond_explosion(x,y,false);
					}
					else 
					{
						level_write_xy(x,y,tile_butterfly_north + (dirOffset+3)%4);
					}
				}
				break;
			case tile_firefly_north:
			case tile_firefly_east:
			case tile_firefly_south:
			case tile_firefly_west:
				//firefly 
				//a) turns to the left and moves, if possible
				//b) moves forward, if possible
				//c) turns to the right otherwise
				{
					uint8_t dirOffset = tile - tile_firefly_north;
					uint8_t dirOffsetLeft= (dirOffset+3)%4;
					uint8_t leftX = x + dx[dirOffsetLeft],leftY = y + dy[dirOffsetLeft%4];

					if(level_read_xy(leftX,leftY) == tile_empty)//tile to the right
					{
						level_write_xy(x,y,tile_firefly_north + dirOffsetLeft);
						swap(x,y,(game_direction)(dirOffsetLeft));
						//HACK to exclude the tile from further processing
						level_write_local_xy(leftX,leftY,tile_firefly_north + dirOffsetLeft+100);
					}
					else if(level_read_xy(leftX,leftY)== tile_player)
					{
						game_die();
						game_diamond_explosion(x,y,false);
					}
					else if(level_read_xy(x+dx[dirOffset],y+dy[dirOffset]) == tile_empty)
					{
						swap(x,y,(game_direction)(dirOffset));
						level_write_local_xy(x+dx[dirOffset],y+dy[dirOffset],tile+100);
					}
					else if(level_read_xy(x+dx[dirOffset],y+dy[dirOffset]) == tile_player)
					{
						game_die();
						game_diamond_explosion(x,y,false);
					}
					else 
					{
						level_write_xy(x,y,tile_firefly_north +(dirOffset+1)%4);
					}
				}
				break;
            case tile_explosion_center2:
            case tile_diamond_explosion_center2:
                level_write_local_xy(x, y, tile-1);
                break;
			case tile_explosion_center:
				game_explosion_cooldown(x,y);
				break;
			case tile_diamond_explosion_center:
				game_diamond_explosion_cooldown(x,y);
				break;
			}
		}
		y--;
	}

	for (y = 0; y < LEVEL_HEIGHT-1; ++y) 
	{
		for (x = 0; x < LEVEL_WIDTH; ++x) 
		{
			const uint8_t tile = level_read_xy(x, y);
			const uint8_t below = level_read_xy(x, y+1);
			if(tile >= 100)
			{
				level_write_local_xy(x,y,tile-100);
			}
			else
				{switch (tile) 
				{
				case tile_stone:
				case tile_diamond:
					if (below == tile_stone || below == tile_diamond) 
					{
					
						const uint8_t left = level_read_xy(x-1, y);
						const uint8_t leftBelow = level_read_xy(x-1, y+1);
						const uint8_t right = level_read_xy(x+1, y);
						const uint8_t rightBelow = level_read_xy(x+1, y+1);
					
						if (left == tile_empty && leftBelow == tile_empty) 
						{
						
							if (isFallingInNeighbourColumn(x, y, -1) ||
								isFallingInThisColumn(x, y)) 
							{
							
								break;
							}
						
							// tile_* -> tile_falling_*
							setToValue(x, y, tile + GAME_FALLING_DIST);
							swap(x, y, dir_west);
						} 
						else if (right == tile_empty &&
								   rightBelow == tile_empty) 
						{
					
							if (isFallingInNeighbourColumn(x, y, 1) ||
								isFallingInThisColumn(x, y)) {
						
								break;
							}
					
							// tile_* -> tile_falling_*
							setToValue(x, y, tile + GAME_FALLING_DIST);
							swap(x, y, dir_east);
						}
					}
					break;
				}
			}
		}
	}
}
