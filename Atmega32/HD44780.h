/*
 * HD44780.h - A driver for HD44780 device with only 4 data lines, RS and E
 *
 * Created: 2013-04-20 16:24:07
 *  Author: Gustaw Smolarczyk
 */

#ifndef HD44780_h
#define HD44780_h

#include "Global.h"

// HD44780 Fcp = 270kHz
#define HD44780_FREQUENCY 270000

// Standard delays (in cycles).
#define HD44780_DELAY_CLEAR 411 // TODO
#define HD44780_DELAY_RETURN 411
#define HD44780_DELAY_NORMAL 10

// Performs a delay in HD44780 cycles.
#define HD44780_DELAY(cycles) _delay_us(((cycles)*1e6)/HD44780_FREQUENCY)

// Select which port is connected to the device.
#define HD44780_PORT D

#define HD44780_RS 2 // 0:   RS
#define HD44780_E  3 // 1:   E
                     // 2-3: Unsued (outputted as 0)
#define HD44780_DH 4 // 4-7: DB4-7
#define HD44780_MASK 0b11111100

// Commands:
#define HD44780_CLEAR_DISPLAY			0b00000001

#define HD44780_RETURN_HOME				0b00000010

#define HD44780_SET_ENTRY_MODE			0b00000100
#define HD44780_SET_ENTRY_MODE_DIRECTION		1
#define HD44780_SET_ENTRY_MODE_DIRECTION_DECREMENT		0
#define HD44780_SET_ENTRY_MODE_DIRECTION_INCREMENT		1
#define HD44780_SET_ENTRY_MODE_SHIFT			0

#define HD44780_DISPLAY_CONTROL			0b00001000
#define HD44780_DISPLAY_CONTROL_DISPLAY			2
#define HD44780_DISPLAY_CONTROL_CURSOR			1
#define HD44780_DISPLAY_CONTROL_BLINK			0

#define HD44780_CURSOR_DISPLAY_SHIFT	0b00010000
#define HD44780_CURSOR_DISPLAY_SHIFT_DISPLAY	3
#define HD44780_CURSOR_DISPLAY_SHIFT_DIRECTION	2
#define HD44780_CURSOR_DISPLAY_SHIFT_DIRECTION_LEFT		0
#define HD44780_CURSOR_DISPLAY_SHIFT_DIRECTION_RIGHT	1

#define HD44780_SET_FUNCTION			0b00100000
#define HD44780_SET_FUNCTION_DATA_LENGTH		4
#define HD44780_SET_FUNCTION_LINES				3
#define HD44780_SET_FUNCTION_LINES_ONE					0
#define HD44780_SET_FUNCTION_LINES_TWO					1
#define HD44780_SET_FUNCTION_FONT				2
#define HD44780_SET_FUNCTION_FONT_5x8					0
#define HD44780_SET_FUNCTION_FONT_5x10					1

#define HD44780_SET_CGRAM_ADDRESS		0b01000000

#define HD44780_SET_DDRAM_ADDRESS		0b10000000

#define _DEFINE_SYNC(name, args, invocation, delay)	\
static inline void hd44780_ ## name ## _sync args {	\
	hd44780_ ## name invocation ;					\
	HD44780_DELAY(delay);							\
}

// Always synchronous.
extern void hd44780_configure(uint8_t lines, uint8_t font);

// Clear the display and return to home.
extern void hd44780_clear_display(void);
_DEFINE_SYNC(clear_display, (void), (), HD44780_DELAY_CLEAR)

// Return to home.
extern void hd44780_return_home(void);
_DEFINE_SYNC(return_home, (void), (), HD44780_DELAY_RETURN)

// Set entry mode.
extern void hd44780_set_entry_mode(uint8_t direction, bool shift);
_DEFINE_SYNC(set_entry_mode, (uint8_t direction, bool shift),
			 (direction, shift), HD44780_DELAY_NORMAL)

// Set display control.
extern void hd44780_display_control(bool display, bool cursor, bool blink);
_DEFINE_SYNC(display_control, (bool display, bool cursor, bool blink),
			 (display, cursor, blink), HD44780_DELAY_NORMAL)

// Shift cursor/display.
extern void hd44780_cursor_display_shift(bool display, uint8_t direction);
_DEFINE_SYNC(cursor_display_shift, (bool display, uint8_t direction),
			 (display, direction), HD44780_DELAY_NORMAL)

// Set display function.
extern void hd44780_set_function(uint8_t lines, uint8_t font);
_DEFINE_SYNC(set_function, (uint8_t lines, uint8_t font),
			 (lines, font), HD44780_DELAY_NORMAL)

// Set CGRAM address.
extern void hd44780_set_cgram_address(uint8_t address);
_DEFINE_SYNC(set_cgram_address, (uint8_t address),
			 (address), HD44780_DELAY_NORMAL)

// Set DDRAM address
extern void hd44780_set_ddram_address(uint8_t address);
_DEFINE_SYNC(set_ddram_address, (uint8_t address),
			 (address), HD44780_DELAY_NORMAL)

// Set DDRAM address on multi-line display.
extern void hd44780_goto(uint8_t x, uint8_t y);
_DEFINE_SYNC(goto, (uint8_t x, uint8_t y),
(x, y), HD44780_DELAY_NORMAL)

// Write data into CGRAM or DDRAM.
extern void hd44780_write_data(uint8_t data);
_DEFINE_SYNC(write_data, (uint8_t data),
			 (data), HD44780_DELAY_NORMAL)

#undef _DEFINE_SYNC

// Always synchronous.
// NOTE: Function assumes, that entry mode direction is increment
//		 and address is set to DDRAM.
extern void hd44780_puts(char* str);

#endif /* HD44780_h */
