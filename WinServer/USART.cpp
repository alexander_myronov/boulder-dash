extern "C" {
#include "USART.h"
}


#include "Global.h"
#include "Server.h"

void usart_configure(void) {
	// nothing
}

bool usart_indirect_complete(void) {
    return true;
}

void usart_next_frame(void) {
    // nothing
}

void usart_direct_putc(char c) {
	Server::instance().comm.write(1,(const void*)&c);
}

void usart_direct_puts(const char* s) {
    Server::instance().comm.write(strlen(s),s);
}

void usart_direct_putCs(const __flash char* s) {
    usart_direct_puts(s);
}

void usart_direct_puts_s(const char* s, uint8_t size) {
    Server::instance().comm.write(size, s);
}

void usart_direct_putCs_s(const __flash char* s, uint8_t size) {
    usart_direct_puts_s(s, size);
}

void usart_indirect_puts(const char* s) {
    usart_direct_puts(s);
}

void usart_indirect_putCs(const __flash char* s) {
    usart_direct_putCs(s);
}

void usart_indirect_puts_s(const char* s, uint16_t size) {
    Server::instance().comm.write(size, s);
}

void usart_indirect_putCs_s(const __flash char* s, uint16_t size) {
    usart_indirect_puts_s(s, size);
}
