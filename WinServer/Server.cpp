#include "Server.h"

extern "C" 
{
#include "BoulderDash.h"
}
#include <process.h>
Server* Server::_instance = nullptr;

void THE_CALLBACK(const void* buffer, int size);

Server::Server(void)
	:comm(wstring(L"COM2"),115200,TWOSTOPBITS,EVENPARITY,8,true,&THE_CALLBACK)
{
	memset(buttons, 0, sizeof buttons);
	hThread = (HANDLE)_beginthreadex(NULL,1024, thread,this,CREATE_SUSPENDED,NULL);
}


Server::~Server(void)
{
	if(hThread != nullptr)
	{
		TerminateThread(hThread,-1);
		hThread = nullptr;
	}
}

Server& Server::instance()
{
	if(_instance == nullptr)
		_instance = new Server;
	return *_instance;
}

void Server::execute()
{
	boulder_dash_init();
	ResumeThread(hThread);
	
	
}


unsigned WINAPI Server::thread(void* param)
{
	boulder_dash_exec();
	return 127;
}

void Server::buttonLoop()
{
	for(int i=0;i<5;i++)
	{
		if(buttons[i] > 0)
			buttons[i]--;
	}
}
void Server::buttonUp()
{
	buttons[0]++;

}

void Server::buttonDown()
{
	buttons[1]++;

}
void Server::buttonLeft()
{
	buttons[2]++;

}
void Server::buttonRight()
{
	buttons[3]++;

}
void Server::buttonR()
{
	buttons[4]++;

}


unsigned Server::getButtons()
{
	unsigned res=0;
	if(buttons[0])
		res |= 1<<11;
	if(buttons[1])
		res |= 1<<10;
	if(buttons[2])
		res |= 1<<14;
	if(buttons[3])
		res |= 1<<6;
	if(buttons[4])
		res |= 1<<12;
	return res;
}