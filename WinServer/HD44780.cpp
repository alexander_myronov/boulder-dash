#include "HD44780Mock.h"

#include <assert.h>
#include <string.h>

extern "C" {
#include "HD44780.h"
}

char hd44780_buffer[0x80];
unsigned hd44780_char_pos, hd44780_window_pos;
bool hd44780_direction, hd44780_shift;
bool hd44780_display, hd44780_cursor, hd44780_blink;

inline void hd44780_adjust_pos(unsigned& pos) {
	unsigned y = pos / 0x40;
	unsigned x = pos % 0x40;

	if (x >= 40) { // 40 decimal!
		x = 0;
		++y;
	}
	if (y >= 2) {
		y = 0;
	}

	pos = y*0x40 + x;
}

void hd44780_configure(uint8_t lines, uint8_t font) {
	hd44780_shift = false;
	hd44780_set_function(lines, font);
	hd44780_display_control(false, false, false);
	hd44780_clear_display();
}

void hd44780_clear_display() {
	memset(hd44780_buffer, ' ', sizeof hd44780_buffer);
	hd44780_direction = true;
	hd44780_return_home();
}

void hd44780_return_home() {
	hd44780_char_pos = hd44780_window_pos = 0;
}

void hd44780_set_entry_mode(uint8_t direction, bool shift) {
	hd44780_direction = !!direction;
	hd44780_shift = shift;
}

void hd44780_display_control(bool display, bool cursor, bool blink) {
	hd44780_display = display;
	hd44780_cursor = cursor;
	hd44780_blink = blink;
}

void hd44780_cursor_display_shift(bool display, uint8_t direction) {
	if (display) {
		if (direction) {
			if (++hd44780_window_pos >= 40) {
				hd44780_window_pos = 0;
			}
		} else {
			if (hd44780_window_pos-- == 0) {
				hd44780_window_pos = 39;
			}
		}
	}

	hd44780_char_pos += (direction ? 1 : -1);

	hd44780_adjust_pos(hd44780_char_pos);
}

void hd44780_set_function(uint8_t lines, uint8_t font) {
	assert(lines == HD44780_SET_FUNCTION_LINES_TWO);
	(void)lines;
	(void)font;
}

void hd44780_set_cgram_address(uint8_t address) {
	assert(!"UNIMPLEMENTED");
}

void hd44780_set_ddram_address(uint8_t address) {
	hd44780_char_pos = address;
}

void hd44780_goto(uint8_t x, uint8_t y) {
	hd44780_set_ddram_address(x + y * 0x40);
}

void hd44780_write_data(uint8_t data) {
	hd44780_buffer[hd44780_char_pos] = data;
	
	hd44780_cursor_display_shift(hd44780_shift, hd44780_direction);
}

void hd44780_puts(char* str) {
	while (*str) {
		hd44780_write_data_sync(*str);
		++str;
	}
}
