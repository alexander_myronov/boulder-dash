extern "C" {
#include "GTerminal.h"
#include "Led.h"
}

volatile bool vsyncFlag;

#include <stdio.h>

void THE_CALLBACK(const void* buffer, int size) {
    for (int i = 0; i < size; ++i) {
        if (((const char*)buffer)[i] == 0) {
            vsyncFlag = true;
        }
    }
}

void gterminal_wait_for_vsync(void) {
    vsyncFlag = false;
	while (!vsyncFlag);
	vsyncFlag = false;
	usart_next_frame();

    if (++secondTimer == 20) {
        ++second;
        secondTimer = 0;
    }
}

uint16_t second = 0;
uint16_t secondTimer = 0;
