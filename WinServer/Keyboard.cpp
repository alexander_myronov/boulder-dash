extern "C" {
#include "Keyboard.h"
}

#include "Server.h"

unsigned keyboard_buttons = 0;

void keyboard_configure(void) {
	// nothing
}

void keyboard_read(void) {
	keyboard_buttons = Server::instance().getButtons();
}
