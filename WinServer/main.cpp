
#include "Server.h"
#include <conio.h>

int main(void)
{
	
	Server::instance().execute();
	while(1)
	{
		
		Server::instance().buttonLoop();
		if(_kbhit())
		{
			int ch1 = _getch();

			if(ch1 == 0xE0)
			{
				int ch2 = _getch();
				switch(ch2)
				{
				case 72: 
					Server::instance().buttonUp();
					break;
				case 80:
					Server::instance().buttonDown();
					break;
				case 75:
					Server::instance().buttonLeft();
					break;
				case 77:
					Server::instance().buttonRight();
					break; 
				}
			}
			else if( ch1 == '\r')
			{
				Server::instance().buttonR();
				break;
			}

		}
		
		Sleep(100);

	}
	return 0;
}