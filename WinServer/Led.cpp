#include <string.h>

extern "C" {
#include "Led.h"
}

unsigned char led_digits[LED_DIGITS_COUNT] = {
	LED_EMPTY, LED_EMPTY, LED_EMPTY, LED_EMPTY
};

unsigned char led_digit_index = LED_DIGITS_COUNT-1;

void led_configure(void) {
	// nothing
}

void led_change_digit(void) {
	led_digit_index = (led_digit_index+1) % LED_DIGITS_COUNT;
}

char led_hex_to_bcd(unsigned number, unsigned char bcd[LED_DIGITS_COUNT]) {
	memset(bcd, LED_EMPTY, LED_DIGITS_COUNT);
	if (number == 0) {
		bcd[0] = 0;
		return 0;
	}
	for (unsigned char i = 0; i < LED_DIGITS_COUNT; ++i) {
		bcd[i] = number & 0xF;
		number >>= 4;
		if (number == 0) {
			return 0;
		}
	}
	
	return 1;
}

char led_dec_to_bcd(unsigned number, unsigned char bcd[LED_DIGITS_COUNT]) {
	memset(bcd, LED_EMPTY, LED_DIGITS_COUNT);
	if (number == 0) {
		bcd[0] = 0;
		return 0;
	}
	for (unsigned char i = 0; i < LED_DIGITS_COUNT; ++i) {
		bcd[i] = number % 10;
		number /= 10;
		if (number == 0) {
			return 0;
		}
	}
	
	return 1;
}
