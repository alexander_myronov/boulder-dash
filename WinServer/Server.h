#pragma once

#include "../ComReader/ComReader.h"
#include <Windows.h>
class Server
{
private:
	static Server* _instance;
	Server(void);
	int buttons[5];
	HANDLE hThread;
	static  unsigned WINAPI thread(void* param);


public:
	static Server& instance();
	
	~Server(void);

	ComReader comm;
	void execute();

	void buttonLoop();
	void buttonUp();
	void buttonDown();
	void buttonLeft();
	void buttonRight();
	void buttonR();

	unsigned getButtons();


};

