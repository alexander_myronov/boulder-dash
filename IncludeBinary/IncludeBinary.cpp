#include "stdafx.h"

#include <string>
#include <fstream>
#include <regex>
#include <sstream>
#include <iomanip>

#include <iostream>

template <typename T, typename U>
T stream_cast(const U& input) {
    T ret;

    std::stringstream ss;
    ss << input;
    ss >> ret;

    return ret;
}

class Generator {
public:
    Generator(const std::wstring& fileName) {
        file.open(fileName.c_str(), std::ios::in | std::ios::binary);
    }

    bool generate() {
        std::regex pattern("\\s*#\\s*include\\s*\"(.+)\\.(\\d+)\\.(\\d+)\\.bin\".*");

        std::string line;
        while (std::getline(file, line)) {
            // Windows...
            if (!line.size()) {
                continue;
            }
            if (line[line.size()-1] == '\n') {
                line.erase(line.size()-1);
            }
            if (!line.size()) {
                continue;
            }
            if (line[line.size()-1] == '\r') {
                line.erase(line.size()-1);
            }

            std::smatch m;
            if (!std::regex_match(line, m, pattern)) {
                continue;
            }

            std::cout << m[1] << ", " << m[2] << ", " << m[3] << '\n';

            if (!generateFile(m[1], stream_cast<unsigned> (m[2]), stream_cast<unsigned> (m[3]))) {
                return false;
            }
        }

        return true;
    }

private:
    std::fstream file;

    bool generateFile(const std::string& fileName, unsigned start, unsigned size) {
        std::fstream input(fileName, std::ios::in | std::ios::binary);

        std::string outputName = fileName
            + "." + stream_cast<std::string>(start)
            + "." + stream_cast<std::string>(size)
            + ".bin";
        std::fstream output(outputName, std::ios::out | std::ios::binary);

        if (size == 0) {
            return false;
        }

        output.setf(std::ios::uppercase);
        output.setf(std::ios::hex, std::ios::basefield);
        output.setf(std::ios::right, std::ios::adjustfield);
        output.fill('0');

        int counter = 0;

        unsigned c;
        while ((c = input.get()) != EOF) {
            if (start > 0) {
                --start;
                continue;
            }
            output << "0x" << std::setw(2) << c << ",";
            if (++counter == 16) {
                counter = 0;
                output << "\n";
            } else {
                output << " ";
            }

            if (--size == 0) {
                break;
            }
        }

        return true;
    }
};

int wmain(int argc, wchar_t* argv[]) {
    try {
        for (int i = 1; i < argc; ++i) {
            std::wstring name(argv[i]);
            Generator generator(name);
            if (!generator.generate()) {
                return 1;
            }
        }
    } catch (...) {
        std::cout << "EXCEPTION\n";
        return 127;
    }

	return 0;
}
