#pragma once
#include <Windows.h>
#include <string>
#include <deque>
#include <functional>
using namespace std;

#define MAXREAD 1024



class ComReader
{
public:
	static void enumeratePorts();
	static deque<wstring> portNames;
	ComReader(const wstring& name,
				int baud,
				BYTE stopBits,BYTE parity,
				BYTE byteSize,
				bool useReadCallback,
				function<void(const void*,int)> cb);
	~ComReader(void);
	
	void write(int bytes,const void *buffer);
	//int ReadBuffer( int bytes,void * pBuffer );

private:
	HANDLE hFile;
	HANDLE hStop;
	HANDLE hTimer;
	HANDLE hReadEvent;
	HANDLE hThread;
	int readWaiting;
	OVERLAPPED olRead,olWrite;
	deque<BYTE> buffer;
	function<void(void*,int)> callback;

	
	static unsigned WINAPI thread(LPVOID param);
	static void CALLBACK timerCallback(LPVOID param, BOOLEAN timerOrWait);
};



