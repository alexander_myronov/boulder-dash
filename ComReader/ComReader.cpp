#include <windows.h>
#include "ComReader.h"
#include <sstream>
#include <deque>
#include <string>
#include <process.h>
std::deque<std::wstring> ComReader::portNames;

#include <fstream>
ofstream loggerRead("logRead.txt",ios::out);
ofstream loggerWrite("logWrite.txt",ios::out);


ComReader::ComReader(const wstring& name,
					 int baud,BYTE stopBits,
					 BYTE parity,BYTE byteSize,
					 bool useReadCallback,
					 function<void(const void*,int)> cb):readWaiting(0),callback(cb)
{
	hFile = CreateFile(
		name.c_str(),
		GENERIC_READ|GENERIC_WRITE,
		0,
		NULL,
		OPEN_EXISTING,
		0,
		NULL
		);
	if(hFile == INVALID_HANDLE_VALUE)
		throw std::exception("CreateFile failed!");
	DCB dcb;
	
	memset(&dcb,0,sizeof(dcb));
	dcb.DCBlength=sizeof(dcb);
	
	if(!GetCommState(hFile,&dcb))
	{
		throw std::exception("get config failed!");
	}

	dcb.BaudRate = baud;
	dcb.StopBits = stopBits;
	dcb.Parity = parity;
	dcb.ByteSize = byteSize;
	dcb.fOutX = FALSE;
	dcb.fParity = parity!=NOPARITY;

	if(!SetCommState(hFile,&dcb))
	{
		DWORD e  = GetLastError();
		throw std::exception("set config failed!");
	}

	
	
	memset(&olWrite,0,sizeof(olWrite));
	memset(&olRead,0,sizeof(olRead));

	
	if(useReadCallback)
	{
		CreateTimerQueueTimer(&hTimer,NULL,timerCallback,this,0,1,WT_EXECUTEDEFAULT);
		hReadEvent = CreateEvent(NULL,false,false,NULL);
		olRead.hEvent = CreateEvent(NULL,false,false,NULL);
		hStop = CreateEvent(NULL,false,false,NULL);
		hThread = (HANDLE)_beginthreadex(NULL,1024,thread,this,0,NULL);

	}
	else
	{
		hStop = hThread = hReadEvent= hTimer = nullptr;
	}
	
}


ComReader::~ComReader(void)
{
	
	
	if(hStop != nullptr && hThread != nullptr)
	{
		SetEvent(hStop);
		WaitForSingleObject(hThread,INFINITE);
		CloseHandle(hThread);
		CloseHandle(hStop);
		hStop = hThread = nullptr;
		if(olRead.hEvent != nullptr)
		{
			CloseHandle(olRead.hEvent);
			olRead.hEvent=NULL;
		}
	}
	if(hTimer != nullptr)
	{
		DeleteTimerQueueTimer(NULL,hTimer,NULL);
		hTimer = nullptr;
	}
	if(hFile!=NULL)
	{
		CloseHandle(hFile);
		hFile=NULL;
	}
	
	if(hReadEvent!=NULL)
	{
		CloseHandle(hReadEvent);
		hReadEvent=NULL;
	}
	
	loggerRead.close();
	loggerWrite.close();
}

void ComReader::enumeratePorts()
{
	portNames.clear();
	for (int i=0;i<255;i++)
	{
		wstringstream s;
		s<<L"COM"<<i+1;
		COMMCONFIG config;
		memset(&config,0,sizeof(config));
		DWORD dwSize=sizeof(config);
		if(GetDefaultCommConfig(s.str().c_str(),&config,&dwSize))
		{
			portNames.push_back(s.str());
		}

	}
	
}


	
	


void ComReader::write(int bytes,const void *buffer)
{
	DWORD read;
	 WriteFile(hFile,buffer,bytes,&read,NULL);
	 loggerWrite.write((const char*)buffer,bytes);

}




unsigned WINAPI ComReader::thread(LPVOID param)
{
	ComReader& reader = *(ComReader*)param;
	HANDLE handles[] = {reader.hStop,reader.hReadEvent,reader.olRead.hEvent};
	DWORD bytesRead=0;
	bool reading=false;
	BYTE buffer[MAXREAD];
	DWORD status;
	COMSTAT comStat;
	DWORD toRead;
	while(1)
	{
		int w=0;

			w = WaitForMultipleObjects(3,handles,false,INFINITE);

		
		
		if(w == WAIT_OBJECT_0)//stop thread
		{
			return 0;
		}
		if(w == WAIT_OBJECT_0+1)
		{
			if(!reading)
			{
				
				if(!ClearCommError(reader.hFile, &status, &comStat))
				{
					toRead = 10;
				}
				else
				{
					if(comStat.cbInQue == 0)
						continue;
					toRead = min(MAXREAD,comStat.cbInQue);

				}

				if(!ReadFile(reader.hFile,&buffer,toRead,&bytesRead,NULL))
					throw new std::exception("read operation failed!");
				if(GetLastError() != ERROR_IO_PENDING)
				{
					if(bytesRead>0)
					{
						loggerRead.write((const char*)buffer,bytesRead);
						reader.callback(buffer,bytesRead);
						reading = false;
					}
				}
				else
					reading=true;
			}
		}
		else if(w == WAIT_OBJECT_0+2)
		{
			if(!GetOverlappedResult(reader.hFile,&reader.olRead,&bytesRead,FALSE))
				continue;
			if(bytesRead>0)
			{
				loggerRead.write((const char*)buffer,bytesRead);
				reader.callback(buffer,bytesRead);
				
			}
			reading = false;
		}
	}
	return 0;

}

void CALLBACK ComReader::timerCallback( LPVOID param, BOOLEAN timerOrWait )
{
	//static int oldTick=GetTickCount();
	ComReader& reader = *(ComReader*)param;
	SetEvent(reader.hReadEvent);
	//int tick = GetTickCount();
	//cout<<tick-oldTick<<endl;
	//oldTick=tick;
}



