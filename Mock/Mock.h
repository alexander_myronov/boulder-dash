#ifndef Mock_h
#define Mock_h

#include "Window.h"

class Mock {
	static Mock* singleton;

public:
	static Mock& instance() { return *singleton; }

	Mock(const std::string& root);

	int execute();

	uint16_t keyboardButtons();

    uint8_t wait();
	void flip();

	void handleEvents(const SDL_Event* event);
	
	Window window;

	struct HD44780_Text : Window::Text {
		int number;
	};

private:
	bool buttons[SDLK_LAST];
	bool hideDisplays;
	HD44780_Text hd44780[2];
	Window::Text led;


	void initHD44780_Line(HD44780_Text& line, int n);
	void initLed();
	
};

#endif
