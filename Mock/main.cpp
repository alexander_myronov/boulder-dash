#include "Mock.h"
#include "Message.h"
#include <SDL_main.h>

static
std::string getProgramDirectory(const char* argv0) {
	std::string root(argv0);
	size_t pos = root.rfind('/');
	if (pos == std::string::npos) {
		pos = root.rfind('\\');
	}
	if (pos == std::string::npos) {
		return ".";
	}

	root.erase(pos);
	return root;
}

int main(int argc, char* argv[]) {
	try {
		Mock mock(getProgramDirectory(argv[0]) + "/..");

		return mock.execute();
	} catch (std::exception& e) {
		Message("Exception") << e.what();
		return 1;
	}
}
