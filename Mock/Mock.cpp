#include "Mock.h"

#include "HD44780Mock.h"

extern "C" {
#include <BoulderDash.h>
#include "Led.h"
}

Mock* Mock::singleton = 0;

static void handleEventProxy(Window*, const SDL_Event* e) {
	Mock::instance().handleEvents(e);
}

static bool HD44780_GetText(const Window::Text& _text, char* buffer) {
	typedef Mock::HD44780_Text HD44780_Text;
	const HD44780_Text& text = (const HD44780_Text&)_text;

	char lastBuffer[16];
	memcpy(lastBuffer, buffer, 16);

	const char* source = hd44780_buffer + text.number*0x40;
	unsigned first = (hd44780_window_pos % 40);
	unsigned last = first + 16;
	if (last <= 40) {
		memcpy(buffer, source + first, 16);
	} else {
		unsigned additional = last - 40;
		memcpy(buffer, source + first, 16 - additional);
		memcpy(buffer + 16 - additional, source, additional);
	}

	buffer[16] = '\0';

	return !!memcmp(buffer, lastBuffer, 16);
}

static bool LED_GetText(const Window::Text& text, char* buffer) {
	char lastBuffer[4];
	memcpy(lastBuffer, buffer, 4);

	for (int i = 0; i < 4; ++i) {
		const uint8_t digit = led_digits[3-i];
		if (digit >= 0 && digit <= 9) {
			buffer[i] = '0' + digit;
		} else if (digit >= 0xA && digit <= 0xF) {
			buffer[i] = 'A' - 0xA + digit;
		} else if (digit == LED_EMPTY) {
			buffer[i] = ' ';
		} else if (digit == LED_UP) {
			buffer[i] = '^';
		} else if (digit == LED_DOWN) {
			buffer[i] = 'v';
		} else {
			buffer[i] = '?';
		}
	}

	buffer[4] = '\0';

	return !!memcmp(buffer, lastBuffer, 4);
}

Mock::Mock(const std::string& root) :
	window(root),
	hideDisplays(false)
	{

	singleton = this;

	memset(buttons, 0, sizeof buttons);
	
	window.setHandleEvent(&handleEventProxy);
	initHD44780_Line(hd44780[0], 0);
	initHD44780_Line(hd44780[1], 1);
	initLed();

	boulder_dash_init();
}

void Mock::initHD44780_Line(HD44780_Text& line, int n) {
	line.number = n;
	line.bufferSize = 17;
	SDL_Color color = {32, 192, 32};
	line.color = color;
	line.position[0] = 20;
	line.position[1] = window.getWindowSize()[1] - 60 + n*25;
	line.getText = &HD44780_GetText;
	line.visible = true;

	window.addText(line);
}

void Mock::initLed() {
	led.bufferSize = 5;
	SDL_Color color = {192, 32, 32};
	led.color = color;
	led.position[0] = 40;
	led.position[1] = window.getWindowSize()[1] - 100;
	led.getText = &LED_GetText;
	led.visible = true;

	window.addText(led);
}

int Mock::execute() {
	try {
		boulder_dash_exec();
        return 127;
	} catch (const char*) {
		return 0;
	}
}

uint16_t Mock::keyboardButtons() {
	uint16_t b = 0;
	if (buttons[SDLK_UP] || buttons[SDLK_KP8]) {
		b |= (1 << 11);
	}
	if (buttons[SDLK_DOWN] || buttons[SDLK_KP2]) {
		b |= (1 << 10);
	}
	if (buttons[SDLK_LEFT] || buttons[SDLK_KP4]) {
		b |= (1 << 14);
	}
	if (buttons[SDLK_RIGHT] || buttons[SDLK_KP6]) {
		b |= (1 << 6);
	}
	if (buttons[SDLK_RETURN] || buttons[SDLK_KP0]) {
		b |= (1 << 12);
	}
	return b;
}

uint8_t Mock::wait() {
    return window.wait();
}

void Mock::flip() {
	if (hd44780_display && !hideDisplays) {
		hd44780[0].visible = true;
		hd44780[1].visible = true;
	} else {
		hd44780[0].visible = false;
		hd44780[1].visible = false;
	}

	if (!hideDisplays) {
		led.visible = true;
	} else {
		led.visible = false;
	}

	if (!window.flip()) {
		throw "END";
	}
}

void Mock::handleEvents(const SDL_Event* event) {
	switch (event->type) {
	case SDL_KEYDOWN:
		if (event->key.keysym.sym == SDLK_TAB && !buttons[SDLK_TAB]) {
			hideDisplays ^= 1;
		}
		buttons[event->key.keysym.sym] = true;
		break;
	case SDL_KEYUP:
		buttons[event->key.keysym.sym] = false;
		break;
	}
}
