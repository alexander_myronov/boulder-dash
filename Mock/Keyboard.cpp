extern "C" {
#include "Keyboard.h"
}

#include "Mock.h"

unsigned keyboard_buttons = 0;

void keyboard_configure(void) {
	// nothing
}

void keyboard_read(void) {
	keyboard_buttons = Mock::instance().keyboardButtons();
}
