#ifndef HD44780Mock_h
#define HD44780Mock_h

extern char hd44780_buffer[0x80];
extern unsigned hd44780_char_pos, hd44780_window_pos;
extern bool hd44780_direction, hd44780_shift;
extern bool hd44780_display, hd44780_cursor, hd44780_blink;

#endif
