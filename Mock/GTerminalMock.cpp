extern "C" {
#include "GTerminal.h"
}

#include "Mock.h"

void gterminal_wait_for_vsync(void) {
    uint8_t count;
    do {
        count = Mock::instance().wait();
        gterminal_divisor_callback(count);
    } while (count != 0);
	Mock::instance().flip();
}
